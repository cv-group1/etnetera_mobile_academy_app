package com.example.etnetera_mobile_academy_app.data.data_src.participant_api

import android.content.Context
import com.example.etnetera_mobile_academy_app.Constants
import com.example.etnetera_mobile_academy_app.data.utils.getHttpClient
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class RetrofitInstance(
    private val context: Context
) {
    private val retrofit by lazy {
        Retrofit.Builder()
            .baseUrl(Constants.ParticipantApi.PARTICIPANT_API_BASE_URL)
            .addConverterFactory(GsonConverterFactory.create())
            .client(getHttpClient(context))
            .build()
    }

    fun getRetrofitInstance(): ParticipantApi = retrofit.create(ParticipantApi::class.java)
}