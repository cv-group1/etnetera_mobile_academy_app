package com.example.etnetera_mobile_academy_app.data.data_src.room_database

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.example.etnetera_mobile_academy_app.domain.models.participant.Homework
import com.example.etnetera_mobile_academy_app.domain.models.participant.Participant
import com.example.etnetera_mobile_academy_app.domain.models.participant.ParticipantType
import com.example.etnetera_mobile_academy_app.domain.models.participant.Skill

@Entity(tableName = "participant_table")
data class ParticipantEntity(
    @PrimaryKey
    val id: String,
    val homework: List<Homework>?,
    val icon192: String,
    val icon512: String,
    val icon72: String,
    val name: String,
    val participantType: ParticipantType?,
    val skills: List<Skill>?,
    val slackURL: String,
    val title: String
)

fun Participant.toEntity() = ParticipantEntity(id, homework, icon192, icon512, icon72, name, participantType, skills, slackURL, title)

fun ParticipantEntity.toModel() = Participant(id, homework, icon192, icon512, icon72, name, participantType, skills, slackURL, title)