package com.example.etnetera_mobile_academy_app.data.data_src.room_database

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import kotlinx.coroutines.flow.Flow

@Dao
interface ParticipantDao {
    @Query("SELECT * FROM participant_table")
    fun getParticipants(): Flow<List<ParticipantEntity>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun addParticipant(participant: ParticipantEntity)

    @Query("DELETE FROM participant_table WHERE id = :id")
    suspend fun deleteParticipant(id: String)

    @Query("SELECT EXISTS(SELECT 1 FROM participant_table WHERE id = :id)")
    suspend fun favoriteCheck(id: String): Boolean?
}