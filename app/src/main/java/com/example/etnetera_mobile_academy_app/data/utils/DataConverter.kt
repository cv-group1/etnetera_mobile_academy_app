package com.example.etnetera_mobile_academy_app.data.utils

import androidx.room.TypeConverter
import com.example.etnetera_mobile_academy_app.domain.models.participant.Homework
import com.example.etnetera_mobile_academy_app.domain.models.participant.ParticipantType
import com.example.etnetera_mobile_academy_app.domain.models.participant.Skill
import com.google.gson.Gson

class DataConverter {
    @TypeConverter
    fun fromSkills(skills: List<Skill>?): String? {
        if(skills == null) return null
        return Gson().toJson(skills)
    }

    @TypeConverter
    fun toSkills(json: String?): List<Skill>? {
        if(json == null) return null
        return Gson().fromJson<List<Skill>>(json, Skill::class.java)
    }

    @TypeConverter
    fun fromHomeworks(homeworks: List<Homework>?): String? {
        if(homeworks == null) return null
        return Gson().toJson(homeworks)
    }

    @TypeConverter
    fun toHomeworks(json: String?): List<Homework>? {
        if(json == null) return null
        return Gson().fromJson<List<Homework>>(json, Homework::class.java)
    }

    @TypeConverter
    fun fromParticipantTypes(types: ParticipantType?): String? {
        if(types == null) return null
        return Gson().toJson(types)
    }

    @TypeConverter
    fun toParticipantTypes(json: String?): ParticipantType? {
        if(json == null) return null
        return Gson().fromJson(json, ParticipantType::class.java)
    }
}