package com.example.etnetera_mobile_academy_app.data.repository

import com.example.etnetera_mobile_academy_app.data.data_src.participant_api.ParticipantApi
import com.example.etnetera_mobile_academy_app.domain.models.login.LoginRequest
import com.example.etnetera_mobile_academy_app.domain.models.login.LoginResponse
import com.example.etnetera_mobile_academy_app.domain.models.participant.Participant
import com.example.etnetera_mobile_academy_app.domain.models.participant.SkillsRequest
import com.example.etnetera_mobile_academy_app.domain.repository.ParticipantRepository

class ParticipantRepositoryImpl(
    private val participantApi: ParticipantApi
): ParticipantRepository {
    override suspend fun getAllParticipants(): Result<List<Participant>> = kotlin.runCatching {
        participantApi.getAllParticipants()
    }

    override suspend fun getParticipant(id: String): Result<Participant> = kotlin.runCatching {
        participantApi.getParticipant(id = id)
    }

    override suspend fun login(userName: String, password: String): Result<LoginResponse> = kotlin.runCatching {
        participantApi.login(LoginRequest(
            user_id = userName,
            password = password
        ))
    }

    override suspend fun updateSkillValues(
        id: String,
        swiftSkill: Int,
        iosSkill: Int,
        kotlinSkill: Int,
        androidSkill: Int
    ) {
        participantApi.updateSkillValues(
            id = id,
            SkillsRequest(
                swift = swiftSkill,
                ios = iosSkill,
                kotlin = kotlinSkill,
                android = androidSkill
            )
        )
    }

    //TODO datastore -> singed user and save current user
}