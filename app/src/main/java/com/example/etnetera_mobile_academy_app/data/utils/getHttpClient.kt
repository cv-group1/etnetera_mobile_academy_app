package com.example.etnetera_mobile_academy_app.data.utils

import android.content.Context
import com.example.etnetera_mobile_academy_app.Constants
import com.example.etnetera_mobile_academy_app.domain.utils.getValueFromSharedPreferences
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import java.util.concurrent.TimeUnit

fun getHttpClient(context: Context): OkHttpClient {
    val interceptor = HttpLoggingInterceptor().apply {
        level = HttpLoggingInterceptor.Level.BODY
    }

    return OkHttpClient.Builder().addInterceptor {
        val accessToken = getValueFromSharedPreferences(context, Constants.ParticipantApi.ACCESS_TOKEN_PREFERENCES)

        val newRequest = it.request().newBuilder().header(
            Constants.ParticipantApi.PARTICIPANT_API_AUTH_HEADER, accessToken
        ).build()
        it.proceed(newRequest)
    }
        .connectTimeout(Constants.ParticipantApi.CONNECTION_TIME_OUT, TimeUnit.SECONDS)
        .readTimeout(Constants.ParticipantApi.CONNECTION_TIME_OUT, TimeUnit.SECONDS)
        .writeTimeout(Constants.ParticipantApi.CONNECTION_TIME_OUT, TimeUnit.SECONDS)
        .retryOnConnectionFailure(false)
        .addInterceptor(interceptor)
        .build()
}