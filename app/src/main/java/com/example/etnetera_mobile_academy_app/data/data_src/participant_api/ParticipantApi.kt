package com.example.etnetera_mobile_academy_app.data.data_src.participant_api

import com.example.etnetera_mobile_academy_app.domain.models.login.LoginRequest
import com.example.etnetera_mobile_academy_app.domain.models.login.LoginResponse
import com.example.etnetera_mobile_academy_app.domain.models.participant.Participant
import com.example.etnetera_mobile_academy_app.domain.models.participant.SkillsRequest
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.POST
import retrofit2.http.Path
import retrofit2.http.Query

interface ParticipantApi {
    @GET("v2/participants")
    suspend fun getAllParticipants(
        @Query("sleepy")
        sleepy: Boolean = false,
        @Query("badServer")
        badServer: Boolean = false
    ): List<Participant>

    @GET("v2/participants/{id}")
    suspend fun getParticipant(
        @Path("id")
        id: String,
        @Query("sleepy")
        sleepy: Boolean = false,
        @Query("badServer")
        badServer: Boolean = false
    ): Participant

    @POST("v2/login")
    suspend fun login(
        @Body body: LoginRequest
    ): LoginResponse

    @POST("v2/participants/{id}/skills")
    suspend fun updateSkillValues(
        @Path("id")
        id: String,
        @Body
        skills: SkillsRequest
    )
}