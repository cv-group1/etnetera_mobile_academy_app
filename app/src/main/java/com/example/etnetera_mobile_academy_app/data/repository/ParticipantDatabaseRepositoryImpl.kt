package com.example.etnetera_mobile_academy_app.data.repository

import com.example.etnetera_mobile_academy_app.data.data_src.room_database.ParticipantDao
import com.example.etnetera_mobile_academy_app.data.data_src.room_database.toEntity
import com.example.etnetera_mobile_academy_app.data.data_src.room_database.toModel
import com.example.etnetera_mobile_academy_app.domain.models.participant.Participant
import com.example.etnetera_mobile_academy_app.domain.repository.ParticipantDatabaseRepository
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.map

class ParticipantDatabaseRepositoryImpl(
    private val participantDao: ParticipantDao
): ParticipantDatabaseRepository {
    override fun getParticipants(): Flow<List<Participant>> =
        participantDao.getParticipants().map { list ->
            list.map { entity ->
                entity.toModel()
            }
        }

    override suspend fun addParticipant(participant: Participant) {
        participantDao.addParticipant(participant.toEntity())
    }

    override suspend fun deleteParticipant(id: String) {
        participantDao.deleteParticipant(id)
    }

    override suspend fun favoriteCheck(id: String): Boolean? =
        participantDao.favoriteCheck(id)

}