package com.example.etnetera_mobile_academy_app

import com.example.etnetera_mobile_academy_app.data.data_src.participant_api.RetrofitInstance
import com.example.etnetera_mobile_academy_app.data.data_src.room_database.AppDatabase
import com.example.etnetera_mobile_academy_app.data.repository.ParticipantDatabaseRepositoryImpl
import com.example.etnetera_mobile_academy_app.data.repository.ParticipantRepositoryImpl
import com.example.etnetera_mobile_academy_app.domain.repository.ParticipantDatabaseRepository
import com.example.etnetera_mobile_academy_app.domain.repository.ParticipantRepository
import com.example.etnetera_mobile_academy_app.domain.use_case.detail.DetailUseCaseContainer
import com.example.etnetera_mobile_academy_app.domain.use_case.detail.FetchParticipantUseCase
import com.example.etnetera_mobile_academy_app.domain.use_case.detail.SaveSkillsUseCase
import com.example.etnetera_mobile_academy_app.domain.use_case.detail.SkillValueChangeUseCase
import com.example.etnetera_mobile_academy_app.domain.use_case.login.FieldsValidationUseCase
import com.example.etnetera_mobile_academy_app.domain.use_case.list.ListFilterUseCase
import com.example.etnetera_mobile_academy_app.domain.use_case.list.ListSearchUseCase
import com.example.etnetera_mobile_academy_app.domain.use_case.list.ListUseCaseContainer
import com.example.etnetera_mobile_academy_app.domain.use_case.login.LoginUseCase
import com.example.etnetera_mobile_academy_app.domain.use_case.login.LoginUseCasesContainer
import com.example.etnetera_mobile_academy_app.presentation.login.AppLoginScreenViewmodel
import com.example.etnetera_mobile_academy_app.presentation.participant_detail.AppDetailViewmodel
import com.example.etnetera_mobile_academy_app.presentation.participants_list.AppListViewmodel
import org.koin.android.ext.koin.androidContext
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

object KoinModules {
    val participantsModul = module {
        viewModel { AppListViewmodel(get(), get()) }
        viewModel { AppDetailViewmodel(get(), get()) }
        viewModel { AppLoginScreenViewmodel(get()) }

        factory {
            LoginUseCasesContainer(
                fieldsValidationUseCase = FieldsValidationUseCase(),
                loginUseCase = LoginUseCase(get(), get(), androidContext())
            )
        }

        factory { FieldsValidationUseCase() }
        factory {
            ListUseCaseContainer(
                listFilterUseCase = ListFilterUseCase(),
                listSearchUseCase = ListSearchUseCase()
            )
        }

        factory {
            DetailUseCaseContainer(
                fetchParticipantUseCase = FetchParticipantUseCase(get(), get()),
                skillValueChangeUseCase = SkillValueChangeUseCase(),
                saveSkillsUseCase = SaveSkillsUseCase(get())
            )
        }

        single { RetrofitInstance(androidContext()).getRetrofitInstance() }
        single<ParticipantRepository> { ParticipantRepositoryImpl(get()) }

        single<ParticipantDatabaseRepository> { ParticipantDatabaseRepositoryImpl(get()) }
        single { AppDatabase.getInstance(androidContext()).participantDao() }
    }
}