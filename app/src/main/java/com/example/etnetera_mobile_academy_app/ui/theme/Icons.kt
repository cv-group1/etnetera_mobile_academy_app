package com.example.etnetera_mobile_academy_app.ui.theme

import androidx.compose.ui.unit.dp
import com.example.etnetera_mobile_academy_app.R
sealed class AppIcons {
    object IconsSVG {
        val kotlin = R.drawable.icon_kotlin
        val android = R.drawable.icon_androidos
        val iOS = R.drawable.icon_ios
        val swift = R.drawable.icon_swift
        val emaSymbol = R.drawable.icon_etnetera_logo
        val Loading = R.drawable.icon_loading
        val Warning = R.drawable.icon_warning
        val statusOK = R.drawable.icon_ok
    }
    object IconSizes{
        val participantIconSize = 48.dp
        val etneteraTopBarSize = 150.dp
        val detailProfilePhotoContainerSize = 280.dp
        val detailProfilePhotoSize = 250.dp
        val coilPlaceholderOnErrorIconSize = 150.dp
        val coilPlaceholderOnLoadingIconSize = 80.dp
        val detailNameIcon = 30.dp
        val skillsSettingIconSize = 25.dp
        val homeworkStatusIconSize = 30.dp
        val textFieldIconSize = 25.dp
        val topBarBackIconSize = 30.dp
    }
    object IconsPNG {
        val emaLogo = R.drawable.etn_logo
    }
}