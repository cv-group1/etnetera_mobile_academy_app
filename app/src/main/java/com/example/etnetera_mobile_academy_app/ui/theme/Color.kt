package com.example.etnetera_mobile_academy_app.ui.theme

import androidx.compose.ui.graphics.Color

val etneteraOrange900 = Color(0xFF2E1003)
val etneteraOrange700 = Color(0xFF8B2F09)
val etneteraOrange600 = Color(0xFFBA3E0C)
val etneteraOrange500 = Color(0xFFE84E0F) // PRIMARY
val etneteraOrange400 = Color(0xFFED713F)
val etneteraOrange200 = Color(0xFFF6B89F)
val etneteraOrange50 = Color(0xFFFDEDE7)

val grey800 = Color(0xFF2E2E2E)
val grey600 = Color(0xFF454545)

val red750 = Color(0xFFC90000)
val red400= Color(0xFFFF4747)
fun listItemShadowColor(color: Color): Color = color.copy(alpha = 0.5f)