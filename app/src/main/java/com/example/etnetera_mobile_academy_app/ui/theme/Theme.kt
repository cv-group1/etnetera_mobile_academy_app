package com.example.etnetera_mobile_academy_app.ui.theme

import android.app.Activity
import android.os.Build
import androidx.compose.foundation.isSystemInDarkTheme
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.darkColorScheme
import androidx.compose.material3.dynamicDarkColorScheme
import androidx.compose.material3.dynamicLightColorScheme
import androidx.compose.material3.lightColorScheme
import androidx.compose.runtime.Composable
import androidx.compose.runtime.SideEffect
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.toArgb
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.platform.LocalView
import androidx.core.view.WindowCompat
import com.google.accompanist.systemuicontroller.rememberSystemUiController

private val DarkColorScheme = darkColorScheme(
    primary = etneteraOrange500,
    onPrimary = etneteraOrange900,
//    primaryVariant = etneteraOrange600,
    secondary = etneteraOrange600,
//    secondaryVariant = grey400,
    background = grey800,
    onBackground = etneteraOrange200,
    surface = grey600,
    onSurface = etneteraOrange200,
    error = red750,
    onError = red400
)

private val LightColorScheme = lightColorScheme(
    primary = etneteraOrange500,
    onPrimary = etneteraOrange900,
//    primaryVariant = etneteraOrange500,
    secondary = etneteraOrange50,
//    secondaryVariant = Color.White,
    background = etneteraOrange50,
    onBackground = etneteraOrange900,
    surface = Color.White,
    onSurface = etneteraOrange900,
    error = red400,
    onError = red750
)

@Composable
fun Etnetera_mobile_academy_appTheme(
    darkTheme: Boolean = isSystemInDarkTheme(),
    // Dynamic color is available on Android 12+
    dynamicColor: Boolean = true,
    content: @Composable () -> Unit
) {
//    val colorScheme = when {
//        dynamicColor && Build.VERSION.SDK_INT >= Build.VERSION_CODES.S -> {
//            val context = LocalContext.current
//            if (darkTheme) dynamicDarkColorScheme(context) else dynamicLightColorScheme(context)
//        }
//
//        darkTheme -> DarkColorScheme
//        else -> LightColorScheme
//    }
    val colorScheme =
        if (!darkTheme) {
            LightColorScheme
        } else {
            DarkColorScheme
        }
    val view = LocalView.current
    if (!view.isInEditMode) {
        SideEffect {
            val window = (view.context as Activity).window
            window.statusBarColor = colorScheme.primary.toArgb()
            WindowCompat.getInsetsController(window, view).isAppearanceLightStatusBars = darkTheme
        }
    }

    val systemUiController = rememberSystemUiController()
    if(darkTheme){
        systemUiController.setSystemBarsColor(
            color = etneteraOrange700
        )
    }else{
        systemUiController.setSystemBarsColor(
            color = etneteraOrange400
        )
    }

    MaterialTheme(
        colorScheme = colorScheme,
        typography = Typography,
        content = content
    )
}