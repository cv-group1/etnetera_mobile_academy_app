package com.example.etnetera_mobile_academy_app.ui.theme

import androidx.compose.material3.Typography
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.font.FontFamily
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp

val listItemTextSize = 28.sp
val topBarTitleSize = 20.sp
val bodyTextSize = 25.sp
val participantDetailTitleSize = 25.sp
val participantDetailNameSize = 30.sp
val homeworkNumberSize = 40.sp
val homeworkStatusSize = 20.sp
val textFieldSize = 20.sp
val errorTextFieldSize = 16.sp
val filterRadioBtnSize = 16.sp
val skillScoreTextSize = 20.sp

val progressIdicatorHeight = 15.dp
val homeworkNumberContainerSize = 70.dp
val homeworkMinimumWidth = 170.dp

// Set of Material typography styles to start with
val Typography = Typography(
    bodyLarge = TextStyle(
        fontFamily = FontFamily.Default,
        fontWeight = FontWeight.Normal,
        fontSize = 16.sp,
        lineHeight = 24.sp,
        letterSpacing = 0.5.sp
    )
    /* Other default text styles to override
    titleLarge = TextStyle(
        fontFamily = FontFamily.Default,
        fontWeight = FontWeight.Normal,
        fontSize = 22.sp,
        lineHeight = 28.sp,
        letterSpacing = 0.sp
    ),
    labelSmall = TextStyle(
        fontFamily = FontFamily.Default,
        fontWeight = FontWeight.Medium,
        fontSize = 11.sp,
        lineHeight = 16.sp,
        letterSpacing = 0.5.sp
    )
    */
)