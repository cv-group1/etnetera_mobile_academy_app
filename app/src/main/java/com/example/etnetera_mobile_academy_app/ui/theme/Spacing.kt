package com.example.etnetera_mobile_academy_app.ui.theme

import androidx.compose.ui.unit.dp

val spaceSmall = 4.dp
val spaceMedium = 8.dp
val spaceLarge = 16.dp
val paddingSpaceHuge = 32.dp

val paddingSpaceZero = 0.dp
val paddingSpaceSmall = 4.dp
val paddingSpaceMedium = 8.dp
val paddingSpaceLarge = 16.dp

val shadowElevationSmall = 4.dp
val shadowElevationMedium = 8.dp
val shadowElevationLarge = 16.dp

val borderLineSmall = 1.dp
val borderLineMedium = 2.dp
val borderDetailPhoto = 5.dp
