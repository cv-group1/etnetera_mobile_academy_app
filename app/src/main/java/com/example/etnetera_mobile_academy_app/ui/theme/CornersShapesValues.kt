package com.example.etnetera_mobile_academy_app.ui.theme

import androidx.compose.ui.unit.dp

const val listItemAndFieldCornerShape = 20
val cornerShapeSmall = 4.dp
val cornerShapeMedium = 8.dp
val cornerShapeLarge = 16.dp