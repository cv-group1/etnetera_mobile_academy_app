package com.example.etnetera_mobile_academy_app.domain.models.participant

data class Participant(
    val id: String,
    val homework: List<Homework>?,
    val icon192: String,
    val icon512: String,
    val icon72: String,
    val name: String,
    val participantType: ParticipantType?,
    val skills: List<Skill>?,
    val slackURL: String,
    val title: String
) {
    fun findSkill(type: SkillType): Skill =
        skills?.find { it.skillType == type.type } ?: Skill(type.type, 0)

    fun doesMatchSearch(input: String): Boolean {
        return name.replace(" ", "").contains(input, ignoreCase = true) ||
                name.split(" ").any { it.startsWith(input, ignoreCase = true) }
    }
}