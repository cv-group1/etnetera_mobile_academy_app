package com.example.etnetera_mobile_academy_app.domain.models.login

data class LoginRequest(
    val user_id: String,
    val password: String
)