package com.example.etnetera_mobile_academy_app.domain.models.participant

import androidx.annotation.DrawableRes
import com.example.etnetera_mobile_academy_app.R
import com.example.etnetera_mobile_academy_app.presentation.utils.UiText
import com.example.etnetera_mobile_academy_app.ui.theme.AppIcons
import com.google.gson.annotations.SerializedName

enum class ParticipantType(
    val typeName: UiText,
    @DrawableRes val typeIcon: Int = AppIcons.IconsSVG.emaSymbol
) {
    @SerializedName("iosStudent")
    STUDENT_IOS(UiText.StringResource(R.string.participant_type_name_ios), AppIcons.IconsSVG.iOS),
    @SerializedName("androidStudent")
    STUDENT_ANDROID(UiText.StringResource(R.string.participant_type_name_android), AppIcons.IconsSVG.android),
    @SerializedName("watcher")
    DEFAULT_ALL(UiText.StringResource(R.string.participant_type_name_default))
}