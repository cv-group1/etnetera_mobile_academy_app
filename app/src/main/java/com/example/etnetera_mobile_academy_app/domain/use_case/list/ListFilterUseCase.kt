package com.example.etnetera_mobile_academy_app.domain.use_case.list

import com.example.etnetera_mobile_academy_app.domain.models.participant.ParticipantType
import com.example.etnetera_mobile_academy_app.presentation.participants_list.ListUiState

class ListFilterUseCase {
    operator fun invoke(uiState: ListUiState, filterType: ParticipantType): ListUiState {
        val participants = uiState.participantsBackup
        return if(filterType == ParticipantType.DEFAULT_ALL){
            uiState.copy(
                participants = participants,
                filterType = filterType,
                searchFieldText = ""
            )
        } else {
            uiState.copy(
                participants = participants.filter { it.participantType == filterType },
                filterType = filterType,
                searchFieldText = ""
            )
        }
    }
}