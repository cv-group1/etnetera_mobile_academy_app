package com.example.etnetera_mobile_academy_app.domain.use_case.login

import com.example.etnetera_mobile_academy_app.domain.models.participant.RegisterResult
import com.example.etnetera_mobile_academy_app.domain.utils.ValidationUtil

class FieldsValidationUseCase{
    operator fun invoke(username: String, password: String): RegisterResult? {
        val usernameResult = ValidationUtil.validateEmail(username)
        val passwordResult = ValidationUtil.validatePassword(password)

        if(usernameResult != null || passwordResult != null){
            return RegisterResult(
                usernameError = usernameResult,
                passwordError = passwordResult
            )
        }
        return null
    }
}