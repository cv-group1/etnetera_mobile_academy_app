package com.example.etnetera_mobile_academy_app.domain.utils.intents

import android.app.Activity
import android.content.Intent
import android.net.Uri
import com.example.etnetera_mobile_academy_app.Constants

fun Activity.getToLinkedInIntent(subject: String? = null) {
    val intentURL = Intent(Intent.ACTION_VIEW, Uri.parse(Constants.LINKED_IN_URL))
    val intentAPP = Intent(Intent.ACTION_SEND).apply {
        subject?.let { putExtra(Intent.EXTRA_TEXT, it) }
        type = Constants.MIME_TYPE_TEXT_PLAIN
        setPackage(Constants.LINKED_IN_PACKAGE_INTENT)
    }
    if (intentAPP.resolveActivity(this.packageManager) != null) {
        this.startActivity(intentAPP)
    } else {
        this.startActivity(intentURL)
    }
}