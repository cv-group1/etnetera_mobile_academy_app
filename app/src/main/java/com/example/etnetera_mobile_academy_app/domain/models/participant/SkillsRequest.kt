package com.example.etnetera_mobile_academy_app.domain.models.participant

data class SkillsRequest(
    val swift: Int,
    val ios: Int,
    val kotlin : Int,
    val android: Int
)