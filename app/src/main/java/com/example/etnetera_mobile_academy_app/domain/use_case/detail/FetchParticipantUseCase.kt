package com.example.etnetera_mobile_academy_app.domain.use_case.detail

import com.example.etnetera_mobile_academy_app.domain.models.participant.SkillType
import com.example.etnetera_mobile_academy_app.domain.repository.ParticipantDatabaseRepository
import com.example.etnetera_mobile_academy_app.domain.repository.ParticipantRepository
import com.example.etnetera_mobile_academy_app.presentation.participant_detail.AppDetailViewmodel
import com.example.etnetera_mobile_academy_app.presentation.participant_detail.DetailUIState

class FetchParticipantUseCase(
    private val repository: ParticipantRepository,
    private val db: ParticipantDatabaseRepository
) {
    suspend operator fun invoke(id: String): AppDetailViewmodel.State {
        repository.getParticipant(id).apply {
            fold(
                onSuccess = { participant ->
                    val skillTypes = SkillType.values()
                    val skills = skillTypes.map { skillType ->
                        participant.findSkill(skillType)
                    }
                    val isFavorite = db.favoriteCheck(id) ?: false

                    return AppDetailViewmodel.State.Success(
                        DetailUIState(
                            participant = participant,
                            isFavorite = isFavorite,
                            skills = skills,
                            backupSkills = skills
                        )
                    )
                },
                onFailure = { error ->
                    return AppDetailViewmodel.State.Error(error)
                }
            )
        }
        return AppDetailViewmodel.State.Loading
    }
}