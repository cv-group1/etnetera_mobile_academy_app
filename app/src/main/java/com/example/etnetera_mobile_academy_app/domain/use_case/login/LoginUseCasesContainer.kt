package com.example.etnetera_mobile_academy_app.domain.use_case.login

data class LoginUseCasesContainer(
    val fieldsValidationUseCase: FieldsValidationUseCase,
    val loginUseCase: LoginUseCase
)