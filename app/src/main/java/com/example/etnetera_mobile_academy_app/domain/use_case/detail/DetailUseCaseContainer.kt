package com.example.etnetera_mobile_academy_app.domain.use_case.detail

class DetailUseCaseContainer(
    val fetchParticipantUseCase: FetchParticipantUseCase,
    val skillValueChangeUseCase: SkillValueChangeUseCase,
    val saveSkillsUseCase: SaveSkillsUseCase
)