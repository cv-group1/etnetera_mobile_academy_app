package com.example.etnetera_mobile_academy_app.domain.use_case.detail

import com.example.etnetera_mobile_academy_app.domain.models.participant.Skill
import com.example.etnetera_mobile_academy_app.domain.models.participant.SkillType
import com.example.etnetera_mobile_academy_app.domain.repository.ParticipantRepository
import com.example.etnetera_mobile_academy_app.presentation.participant_detail.DetailUIState

class SaveSkillsUseCase(
    private val repository: ParticipantRepository
) {
    suspend operator fun invoke(uiState: DetailUIState){
        val participant = uiState.participant
        val skills = uiState.skills
        if (participant != null) {
            repository.updateSkillValues(
                id = participant.id,
                swiftSkill = findValue(skills, SkillType.SWIFT),
                iosSkill = findValue(skills, SkillType.IOS),
                kotlinSkill = findValue(skills, SkillType.KOTLIN),
                androidSkill = findValue(skills, SkillType.ANDROID)
            )
        }
    }
}

private fun findValue(skills: List<Skill>, type: SkillType): Int = skills.find { it.skillType == type.type }?.value ?: 0