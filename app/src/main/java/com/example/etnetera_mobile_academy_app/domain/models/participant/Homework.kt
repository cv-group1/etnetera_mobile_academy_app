package com.example.etnetera_mobile_academy_app.domain.models.participant

data class Homework(
    val id: Int,
    val number: Int,
    val state: String
)