package com.example.etnetera_mobile_academy_app.domain.models.login

data class LoginResponse(
    val expiration: String,
    val access_token: String
)