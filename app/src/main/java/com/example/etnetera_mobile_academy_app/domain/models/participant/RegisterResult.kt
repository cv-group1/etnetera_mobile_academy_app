package com.example.etnetera_mobile_academy_app.domain.models.participant

import com.example.etnetera_mobile_academy_app.presentation.utils.UiText

data class RegisterResult(
    val usernameError: UiText?= null,
    val passwordError: UiText? = null
)
