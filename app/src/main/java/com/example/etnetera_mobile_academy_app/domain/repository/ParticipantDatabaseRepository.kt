package com.example.etnetera_mobile_academy_app.domain.repository

import com.example.etnetera_mobile_academy_app.domain.models.participant.Participant
import kotlinx.coroutines.flow.Flow

interface ParticipantDatabaseRepository {

    fun getParticipants(): Flow<List<Participant>>

    suspend fun addParticipant(participant: Participant)

    suspend fun deleteParticipant(id: String)

    suspend fun favoriteCheck(id: String): Boolean?
}