package com.example.etnetera_mobile_academy_app.domain.models.participant

import com.example.etnetera_mobile_academy_app.Constants

enum class SkillType(val type: String){
   SWIFT(Constants.ParticipantApi.ReturnTypes.SkillName.SWIFT),
   IOS(Constants.ParticipantApi.ReturnTypes.SkillName.IOS),
   KOTLIN(Constants.ParticipantApi.ReturnTypes.SkillName.KOTLIN),
   ANDROID(Constants.ParticipantApi.ReturnTypes.SkillName.ANDROID)
}
