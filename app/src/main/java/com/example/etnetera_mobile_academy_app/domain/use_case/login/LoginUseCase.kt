package com.example.etnetera_mobile_academy_app.domain.use_case.login

import android.content.Context
import com.example.etnetera_mobile_academy_app.Constants
import com.example.etnetera_mobile_academy_app.domain.repository.ParticipantRepository
import com.example.etnetera_mobile_academy_app.domain.utils.saveValueToSharedPreferences
import com.example.etnetera_mobile_academy_app.presentation.login.AppLoginScreenViewmodel
import com.example.etnetera_mobile_academy_app.presentation.login.LoginUiState

class LoginUseCase(
    private val fieldsValidationUseCase: FieldsValidationUseCase,
    private val repository: ParticipantRepository,
    private val context: Context
) {
    suspend operator fun invoke(
        state: LoginUiState,
        onSuccessLogin: () -> Unit
    ): AppLoginScreenViewmodel.State {
        val userName = state.usernameText
        val password = state.passwordText

        val emailValidationResult = fieldsValidationUseCase(userName, password)

        if (emailValidationResult?.usernameError != null ||
            emailValidationResult?.passwordError != null
        ) {
            return AppLoginScreenViewmodel.State.Success(
                uiState = state.copy(
                    usernameError = emailValidationResult.usernameError,
                    passwordError = emailValidationResult.passwordError
                )
            )
        }
        repository.login(userName = userName, password = password).fold(
            onSuccess = { loginRespose ->
                saveValueToSharedPreferences(
                    context = context,
                    key = Constants.ParticipantApi.ACCESS_TOKEN_PREFERENCES,
                    value = loginRespose.access_token
                )
                saveValueToSharedPreferences(
                    context = context,
                    key = Constants.CURRENT_USER_ID,
                    value = userName
                )
                onSuccessLogin()
            },
            onFailure = { error ->
                return AppLoginScreenViewmodel.State.Error(error)
            }
        )

        return AppLoginScreenViewmodel.State.Loading
    }
}