package com.example.etnetera_mobile_academy_app.domain.models.participant

data class Skill(
    val skillType: String,
    val value: Int
)