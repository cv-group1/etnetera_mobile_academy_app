package com.example.etnetera_mobile_academy_app.domain.utils.intents

import android.app.Activity
import android.content.Intent
import android.net.Uri
import com.example.etnetera_mobile_academy_app.Constants

fun Activity.getSendToSlackIntent(slackURL: String) {
    val intentAPP = Intent(Intent.ACTION_VIEW, Uri.parse(slackURL))
    val intentURL = Intent(Intent.ACTION_VIEW, Uri.parse(Constants.SLACK_PACKAGE_INTENT))
    if (intentAPP.resolveActivity(packageManager) != null) {
        startActivity(intentAPP)
    } else {
        startActivity(intentURL)
    }
}