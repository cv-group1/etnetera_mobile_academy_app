package com.example.etnetera_mobile_academy_app.domain.utils.intents

import android.app.Activity
import android.content.Intent
import android.net.Uri
import com.example.etnetera_mobile_academy_app.Constants

fun Activity.getSendToMailIntent(
    sendTo: String? = null,
    subject: String? = null
) {
    val intent = Intent(Intent.ACTION_SENDTO).apply {
        data = Uri.parse(Constants.INTENT_DATA_URI_MAIL_TO)
        sendTo?.let { putExtra(Intent.EXTRA_EMAIL, it) }
        subject?.let { putExtra(Intent.EXTRA_SUBJECT, it) }
    }
    if (intent.resolveActivity(packageManager) != null) {
        startActivity(intent)
    }
}