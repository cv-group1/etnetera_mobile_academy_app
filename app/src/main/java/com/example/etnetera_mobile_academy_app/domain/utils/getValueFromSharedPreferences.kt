package com.example.etnetera_mobile_academy_app.domain.utils

import android.content.Context

fun getValueFromSharedPreferences(context: Context, key: String): String =
    context.getSharedPreferences(key, Context.MODE_PRIVATE).getString(key, "").toString()