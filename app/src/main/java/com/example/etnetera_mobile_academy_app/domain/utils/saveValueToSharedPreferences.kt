package com.example.etnetera_mobile_academy_app.domain.utils

import android.content.Context

fun saveValueToSharedPreferences(context: Context, key: String, value: String) {
    context.getSharedPreferences(key, Context.MODE_PRIVATE).edit().apply {
        putString(key, value)
        apply()
    }
}