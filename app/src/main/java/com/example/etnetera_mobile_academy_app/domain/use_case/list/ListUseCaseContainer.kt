package com.example.etnetera_mobile_academy_app.domain.use_case.list

class ListUseCaseContainer(
    val listFilterUseCase: ListFilterUseCase,
    val listSearchUseCase: ListSearchUseCase
)