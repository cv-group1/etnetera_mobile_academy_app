package com.example.etnetera_mobile_academy_app.domain.use_case.list

import com.example.etnetera_mobile_academy_app.domain.models.participant.ParticipantType
import com.example.etnetera_mobile_academy_app.presentation.participants_list.ListUiState

class ListSearchUseCase {
    operator fun invoke(uiState: ListUiState, text: String): ListUiState {
        val participants = if(uiState.searchFieldText.isBlank()) uiState.participantsBackup
        else uiState.participantsBackup.filter { it.doesMatchSearch(text) }

        return uiState.copy(
            participants = participants,
            searchFieldText = text,
            filterType = ParticipantType.DEFAULT_ALL
        )
    }
}