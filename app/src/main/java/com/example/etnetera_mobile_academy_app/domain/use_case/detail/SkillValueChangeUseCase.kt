package com.example.etnetera_mobile_academy_app.domain.use_case.detail

import com.example.etnetera_mobile_academy_app.domain.models.participant.Skill
import com.example.etnetera_mobile_academy_app.presentation.participant_detail.DetailUIState

class SkillValueChangeUseCase {
    operator fun invoke(
        value: Int,
        type: String,
        uiState: DetailUIState
    ): DetailUIState {
        val skills = uiState.skills.map {
            if (it.skillType == type) Skill(
                skillType = it.skillType, value = value
            ) else it
        }
        return uiState.copy(
            skills = skills
        )
    }
}