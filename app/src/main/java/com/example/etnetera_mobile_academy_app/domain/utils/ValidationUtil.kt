package com.example.etnetera_mobile_academy_app.domain.utils

import com.example.etnetera_mobile_academy_app.Constants
import com.example.etnetera_mobile_academy_app.presentation.utils.InternalErrors
import com.example.etnetera_mobile_academy_app.presentation.utils.UiText

object ValidationUtil{
    fun validateEmail(username: String): UiText? {
        return when{
            username.isBlank() -> {
                InternalErrors.BLANK_FIELD.uiText
            }
            username.trim().compareTo(Constants.ParticipantApi.MY_USERNAME, true) != 0 -> {
                //TODO compare username with participant id
                InternalErrors.USERNAME_INCORRECT_TEXT.uiText
            }
            else -> {
                null
            }
        }
    }

    fun validatePassword(password: String): UiText? {
        return when{
            password.isBlank() -> {
                InternalErrors.BLANK_FIELD.uiText
            }
            password != Constants.ParticipantApi.MY_PASSWORD -> {
                //TODO compare password with participant password
                InternalErrors.PASSWORD_INCORRECT_TEXT.uiText
            }
            else -> {
                null
            }
        }
    }
}