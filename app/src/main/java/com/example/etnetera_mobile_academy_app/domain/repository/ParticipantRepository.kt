package com.example.etnetera_mobile_academy_app.domain.repository

import com.example.etnetera_mobile_academy_app.domain.models.login.LoginResponse
import com.example.etnetera_mobile_academy_app.domain.models.participant.Participant

interface ParticipantRepository {
    suspend fun getAllParticipants(): Result<List<Participant>>

    suspend fun getParticipant(id: String): Result<Participant>

    suspend fun login(userName: String, password: String): Result<LoginResponse>

    suspend fun updateSkillValues(
        id: String,
        swiftSkill: Int,
        iosSkill: Int,
        kotlinSkill: Int,
        androidSkill: Int
    )
}