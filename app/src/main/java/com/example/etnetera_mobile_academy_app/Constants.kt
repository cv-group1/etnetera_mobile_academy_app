package com.example.etnetera_mobile_academy_app

object Constants {
    const val LINKED_IN_URL = "https://cz.linkedin.com/"
    const val MIME_TYPE_TEXT_PLAIN = "text/plain"
    const val LINKED_IN_PACKAGE_INTENT = "com.linkedin.android"
    const val SLACK_PACKAGE_INTENT = "https://slack.com/"
    const val INTENT_DATA_URI_MAIL_TO = "mailto:"
    const val EMAIL_VALIDATION_REGEX_PATTERN = "^[A-Za-z0-9+_.-]+@[^@]+$"
    const val PARTICIPANT_ID_ROUTE_ARG = "participantID"
    const val LOGIN_FEATURE = "login_feature"
    const val DETAIL_FEATURE = "detail_feature"
    const val CURRENT_USER_ID = ""

    const val MAXIMUM_SKILL_VALUE = 10

    object ParticipantApi{
        const val PARTICIPANT_API_AUTH_HEADER = "access_token"
        const val PARTICIPANT_API_BASE_URL = "http://emarest.cz.mass-php-1.mit.etn.cz/api/"
        const val CONNECTION_TIME_OUT: Long = 10
        const val ACCESS_TOKEN_PREFERENCES = "accessTokenPreferences"

        const val MY_USERNAME = "suby91"
        const val MY_PASSWORD = "tu6Q7f2w"

        object ReturnTypes{
            object SkillName{
                const val SWIFT = "swift"
                const val IOS = "ios"
                const val KOTLIN = "kotlin"
                const val ANDROID = "android"
            }

            object HomeworkState{
                const val ACCEPTANCE = "acceptance"
                const val READY = "ready"
                const val COMINGSOON = "comingsoon"
            }

        }
    }
}