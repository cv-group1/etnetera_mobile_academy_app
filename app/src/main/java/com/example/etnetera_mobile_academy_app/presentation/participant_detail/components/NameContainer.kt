package com.example.etnetera_mobile_academy_app.presentation.participant_detail.components

import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import com.example.etnetera_mobile_academy_app.presentation.participant_detail.DetailUIState
import com.example.etnetera_mobile_academy_app.presentation.utils.AppText
import com.example.etnetera_mobile_academy_app.presentation.utils.getParticipantTypeIcon
import com.example.etnetera_mobile_academy_app.ui.theme.paddingSpaceSmall
import com.example.etnetera_mobile_academy_app.ui.theme.participantDetailNameSize

@Composable
fun NameContainer(
    uiState: DetailUIState,
    modifier: Modifier = Modifier
) {
    uiState.participant?.let { participant ->
        Column(
            horizontalAlignment = Alignment.CenterHorizontally,
            verticalArrangement = Arrangement.Center,
            modifier = Modifier
                .fillMaxWidth()
                .then(modifier)
        ) {
            AppText(
                text = participant.name,
                textAlign = TextAlign.Center,
                fontSize = participantDetailNameSize,
                fontWeight = FontWeight.Bold,
                maxLines = 2
            )

            Spacer(modifier = Modifier.height(paddingSpaceSmall))

            DevSpecializationContainer(
                title = participant.title,
                systemIcon = getParticipantTypeIcon(participant.participantType)
            )
        }
    }
}
