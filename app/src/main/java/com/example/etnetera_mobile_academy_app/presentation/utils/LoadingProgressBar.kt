package com.example.etnetera_mobile_academy_app.presentation.utils

import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.padding
import androidx.compose.material3.CircularProgressIndicator
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import com.example.etnetera_mobile_academy_app.ui.theme.paddingSpaceZero

@Composable
fun LoadingProgressBar(
    modifier: Modifier = Modifier,
    paddingValues: PaddingValues = PaddingValues(paddingSpaceZero)
) {
    Box(
        contentAlignment = Alignment.Center,
        modifier = Modifier
            .padding(paddingValues)
            .fillMaxSize()
            .then(modifier)
    ) {
        CircularProgressIndicator()
    }
}