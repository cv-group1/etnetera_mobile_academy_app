package com.example.etnetera_mobile_academy_app.presentation.participant_detail.components

import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.lazy.LazyRow
import androidx.compose.foundation.lazy.items
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import com.example.etnetera_mobile_academy_app.R
import com.example.etnetera_mobile_academy_app.presentation.participant_detail.DetailUIState
import com.example.etnetera_mobile_academy_app.presentation.utils.AppText
import com.example.etnetera_mobile_academy_app.ui.theme.paddingSpaceLarge
import com.example.etnetera_mobile_academy_app.ui.theme.spaceLarge

@Composable
fun HomeworksContainer(
    uiState: DetailUIState,
    modifier: Modifier = Modifier
) {
    Column(
        modifier = Modifier
            .fillMaxWidth()
            .padding(paddingSpaceLarge)
            .then(modifier)
    ) {
        AppText(text = stringResource(R.string.homeworks_title))

        Spacer(modifier = Modifier.height(spaceLarge))

        LazyRow(
            verticalAlignment = Alignment.CenterVertically,
            horizontalArrangement = Arrangement.spacedBy(spaceLarge)
        ) {
            uiState.participant?.homework?.let { homeworks ->
                items(homeworks) { homework ->
                    HomeworkItemContainer(homework = homework)
                }
            }
        }
    }
}