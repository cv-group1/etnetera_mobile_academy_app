package com.example.etnetera_mobile_academy_app.presentation.participant_detail.components

import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Row
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.painter.Painter
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import com.example.etnetera_mobile_academy_app.R
import com.example.etnetera_mobile_academy_app.presentation.utils.AppIcon
import com.example.etnetera_mobile_academy_app.presentation.utils.AppText
import com.example.etnetera_mobile_academy_app.ui.theme.AppIcons
import com.example.etnetera_mobile_academy_app.ui.theme.participantDetailTitleSize

@Composable
fun DevSpecializationContainer(
    title: String,
    modifier: Modifier = Modifier,
    systemIcon: Painter = painterResource(AppIcons.IconsSVG.emaSymbol)
) {
    Row(
        horizontalArrangement = Arrangement.Center,
        verticalAlignment = Alignment.CenterVertically,
        modifier = modifier
    ) {
        AppText(
            text = getTitle(title),
            fontSize = participantDetailTitleSize
        )

        AppIcon(
            icon = systemIcon,
            contentDescription = stringResource(R.string.participant_detail_title_icon_description),
            iconSize = AppIcons.IconSizes.detailNameIcon,
            isClickable = false
        )
    }
}

private fun getTitle(title: String): String = title.replace("#", "")