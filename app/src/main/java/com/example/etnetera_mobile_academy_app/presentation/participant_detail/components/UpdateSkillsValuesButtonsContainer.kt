package com.example.etnetera_mobile_academy_app.presentation.participant_detail.components

import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.width
import androidx.compose.material3.ButtonDefaults
import androidx.compose.material3.MaterialTheme
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import com.example.etnetera_mobile_academy_app.R
import com.example.etnetera_mobile_academy_app.presentation.participant_detail.AppDetailViewmodel
import com.example.etnetera_mobile_academy_app.presentation.utils.AppButton
import com.example.etnetera_mobile_academy_app.ui.theme.spaceLarge

@Composable
fun UpdateSkillsValuesButtonsContainer(
    viewmodel: AppDetailViewmodel, modifier: Modifier = Modifier
) {
    Row(
        horizontalArrangement = Arrangement.Center,
        verticalAlignment = Alignment.CenterVertically,
        modifier = Modifier
            .fillMaxWidth()
            .then(modifier)
    ) {
        AppButton(
            btnText = stringResource(R.string.uprade_skills_values_btn_save_text),
            modifier = Modifier.weight(1f),
            onClick = {
                viewmodel.saveSkillsValues()
                viewmodel.openAndCloseSkillSettingSheet()
            }
        )

        Spacer(modifier = Modifier.width(spaceLarge))

        AppButton(
            btnText = stringResource(R.string.uprade_skills_values_btn_cancel_text),
            modifier = Modifier.weight(1f),
            colors = ButtonDefaults.buttonColors(
                containerColor = MaterialTheme.colorScheme.secondary
            ),
            onClick = {
                viewmodel.cancelSkillUpdate()
                viewmodel.openAndCloseSkillSettingSheet()
            }
        )
    }
}