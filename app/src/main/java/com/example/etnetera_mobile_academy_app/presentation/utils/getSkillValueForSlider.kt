package com.example.etnetera_mobile_academy_app.presentation.utils

import com.example.etnetera_mobile_academy_app.Constants

fun getSkillValueForSlider(value: Int): Float = value.toFloat() / Constants.MAXIMUM_SKILL_VALUE