package com.example.etnetera_mobile_academy_app.presentation.utils

import android.content.Context
import androidx.compose.runtime.Composable
import androidx.compose.ui.layout.ContentScale
import coil.compose.AsyncImagePainter
import coil.compose.rememberAsyncImagePainter
import coil.request.ImageRequest

@Composable
fun getAsyncImgPhoto(
    url: String,
    context: Context,
    contentScale: ContentScale = ContentScale.Fit
): AsyncImagePainter {

    val painter = ImageRequest.Builder(context)
        .data(url)
        .crossfade(1000)
        .build()

    return rememberAsyncImagePainter(model = painter, contentScale = contentScale)
}