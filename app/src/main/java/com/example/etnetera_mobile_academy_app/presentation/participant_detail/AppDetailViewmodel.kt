package com.example.etnetera_mobile_academy_app.presentation.participant_detail

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.etnetera_mobile_academy_app.domain.repository.ParticipantDatabaseRepository
import com.example.etnetera_mobile_academy_app.domain.use_case.detail.DetailUseCaseContainer
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.flow.update
import kotlinx.coroutines.launch

class AppDetailViewmodel(
    private val dbRepository: ParticipantDatabaseRepository,
    private val detailUseCaseContainer: DetailUseCaseContainer
) : ViewModel() {

    private val _state = MutableStateFlow<State>(State.Loading)
    val state = _state.asStateFlow()

    fun fetchParticipant(id: String) {
        viewModelScope.launch {
            _state.value = detailUseCaseContainer.fetchParticipantUseCase(id)
        }
    }

    fun skillValueChange(value: Int, type: String) {
        viewModelScope.launch {
            _state.update { state ->
                (state as State.Success).copy(
                    uiState = detailUseCaseContainer.skillValueChangeUseCase(
                        value = value,
                        type = type,
                        uiState = state.uiState
                    )
                )
            }
        }
    }

    fun cancelSkillUpdate(){
        viewModelScope.launch {
            _state.update {  state ->
                (state as State.Success).copy(
                    uiState = state.uiState.copy(
                        skills = state.uiState.backupSkills
                    )
                )
            }
        }
    }

    fun saveSkillsValues(){
        viewModelScope.launch {
            val uiState = (_state.value as State.Success).uiState
            detailUseCaseContainer.saveSkillsUseCase(uiState)
        }
    }

    fun saveParticipantToDatabase() {
        viewModelScope.launch {
            val participant = (_state.value as State.Success).uiState.participant
            participant?.let { value ->
                dbRepository.addParticipant(value)
                changeIsFavoriteValue()
            }
        }
    }

    fun deleteParticipantFromDatabase() {
        viewModelScope.launch {
            val id = (_state.value as State.Success).uiState.participant?.id
            if(id != null) {
                dbRepository.deleteParticipant(id)
                changeIsFavoriteValue()
            }
        }
    }

    fun openAndCloseSkillSettingSheet() {
        viewModelScope.launch {
            _state.update { state ->
                (state as State.Success).copy(
                    uiState = state.uiState.copy(
                        isSkillChangeSheetOpen = !state.uiState.isSkillChangeSheetOpen
                    )
                )
            }
        }
    }

    private fun changeIsFavoriteValue(){
        viewModelScope.launch {
            _state.update { state ->
                (state as State.Success).copy(
                    uiState = state.uiState.copy(
                        isFavorite = !state.uiState.isFavorite
                    )
                )
            }
        }
    }


    sealed interface State {
        object Loading : State
        data class Success(val uiState: DetailUIState) : State
        data class Error(val error: Throwable) : State
    }
}