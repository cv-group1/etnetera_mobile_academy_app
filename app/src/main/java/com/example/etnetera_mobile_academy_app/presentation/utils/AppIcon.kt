package com.example.etnetera_mobile_academy_app.presentation.utils

import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.material3.Icon
import androidx.compose.material3.MaterialTheme
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.ImageBitmap
import androidx.compose.ui.graphics.painter.Painter
import androidx.compose.ui.graphics.vector.ImageVector
import androidx.compose.ui.unit.Dp
import com.example.etnetera_mobile_academy_app.ui.theme.AppIcons
import com.example.etnetera_mobile_academy_app.ui.theme.paddingSpaceZero
@Composable
fun AppIcon(
    icon: ImageVector,
    modifier: Modifier = Modifier,
    isClickable: Boolean = true,
    onClick: () -> Unit = {},
    paddingValues: PaddingValues = PaddingValues(paddingSpaceZero),
    backgroundColor: Color = Color.Unspecified,
    contentDescription: String? = null,
    iconSize: Dp = AppIcons.IconSizes.participantIconSize,
    tint: Color = MaterialTheme.colorScheme.primary
) {
    Icon(
        imageVector = icon,
        contentDescription = contentDescription,
        tint = tint,
        modifier = Modifier
            .clip(CircleShape)
            .isClickable(isClickable, onClick)
            .background(color = backgroundColor)
            .padding(paddingValues)
            .size(iconSize)
            .then(modifier)
    )
}

@Composable
fun AppIcon(
    icon: Painter,
    modifier: Modifier = Modifier,
    isClickable: Boolean = true,
    onClick: () -> Unit = {},
    paddingValues: PaddingValues = PaddingValues(paddingSpaceZero),
    backgroundColor: Color = Color.Transparent,
    contentDescription: String? = null,
    iconSize: Dp = AppIcons.IconSizes.participantIconSize,
    tint: Color = MaterialTheme.colorScheme.primary
) {
    Icon(
        painter = icon,
        contentDescription = contentDescription,
        tint = tint,
        modifier = Modifier
            .clip(CircleShape)
            .isClickable(isClickable, onClick)
            .background(color = backgroundColor)
            .padding(paddingValues)
            .size(iconSize)
            .then(modifier)
    )
}

@Composable
fun AppIcon(
    icon: ImageBitmap,
    modifier: Modifier = Modifier,
    isClickable: Boolean = true,
    onClick: () -> Unit = {},
    paddingValues: PaddingValues = PaddingValues(paddingSpaceZero),
    backgroundColor: Color = Color.Transparent,
    contentDescription: String? = null,
    iconSize: Dp = AppIcons.IconSizes.participantIconSize,
    tint: Color = MaterialTheme.colorScheme.primary
) {
    Icon(
        bitmap = icon,
        contentDescription = contentDescription,
        tint = tint,
        modifier = Modifier
            .clip(CircleShape)
            .isClickable(isClickable, onClick)
            .background(color = backgroundColor)
            .padding(paddingValues)
            .size(iconSize)
            .then(modifier)
    )
}
private fun Modifier.isClickable(
    isClickable: Boolean,
    onClick: () -> Unit = {}
): Modifier = then(if (isClickable) Modifier.clickable { onClick() } else Modifier)