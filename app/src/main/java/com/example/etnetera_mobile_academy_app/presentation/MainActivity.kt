package com.example.etnetera_mobile_academy_app.presentation

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Surface
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.tooling.preview.Preview
import com.example.etnetera_mobile_academy_app.domain.utils.intents.getSendToMailIntent
import com.example.etnetera_mobile_academy_app.domain.utils.intents.getSendToSlackIntent
import com.example.etnetera_mobile_academy_app.domain.utils.intents.getToLinkedInIntent
import com.example.etnetera_mobile_academy_app.ui.theme.Etnetera_mobile_academy_appTheme

class MainActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            Etnetera_mobile_academy_appTheme {
                // A surface container using the 'background' color from the theme
                Surface(
                    modifier = Modifier.fillMaxSize(),
                    color = MaterialTheme.colorScheme.background
                ) {
                    NavGraph(
                        onSlackClick = { getSendToSlackIntent(it) },
                        onLinkedInClick = { getToLinkedInIntent(it) },
                        onMailClick = { sendTo, subject -> getSendToMailIntent(sendTo, subject) }
                    )
                }
            }
        }
    }
}

@Preview(showBackground = true)
@Composable
fun GreetingPreview() {
    Etnetera_mobile_academy_appTheme {

    }
}