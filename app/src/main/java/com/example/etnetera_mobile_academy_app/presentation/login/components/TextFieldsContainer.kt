package com.example.etnetera_mobile_academy_app.presentation.login.components

import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.text.KeyboardActions
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Email
import androidx.compose.material.icons.filled.Lock
import androidx.compose.material3.MaterialTheme
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.input.ImeAction
import androidx.compose.ui.text.input.KeyboardType
import androidx.compose.ui.text.input.PasswordVisualTransformation
import com.example.etnetera_mobile_academy_app.R
import com.example.etnetera_mobile_academy_app.presentation.login.AppLoginScreenViewmodel
import com.example.etnetera_mobile_academy_app.presentation.utils.AppText
import com.example.etnetera_mobile_academy_app.presentation.utils.AppTextField
import com.example.etnetera_mobile_academy_app.ui.theme.errorTextFieldSize
import com.example.etnetera_mobile_academy_app.ui.theme.spaceSmall

@Composable
fun TextFieldsContainer(
    viewmodel: AppLoginScreenViewmodel,
    state: AppLoginScreenViewmodel.State.Success,
    onSuccessLogin: () -> Unit,
    modifier: Modifier = Modifier
) {
    Column(
        horizontalAlignment = Alignment.CenterHorizontally,
        verticalArrangement = Arrangement.Center,
        modifier = Modifier
            .fillMaxSize()
            .then(modifier)
    ) {
        AppTextField(
            value = state.uiState.usernameText,
            onValueChange = viewmodel::emailFieldTextChange,
            label = stringResource(R.string.e_mail_placeholder_text),
            leadingIcon = Icons.Default.Email
        )
        state.uiState.usernameError?.let { errorMsg ->
            Spacer(modifier = Modifier.height(spaceSmall))
            AppText(
                text = errorMsg,
                color = MaterialTheme.colorScheme.error,
                fontSize = errorTextFieldSize
            )
        }

        Spacer(modifier = Modifier.height(spaceSmall))

        AppTextField(
            value = state.uiState.passwordText,
            onValueChange = viewmodel::passwordFieldTextChange,
            label = stringResource(R.string.password_placeholder_text),
            leadingIcon = Icons.Default.Lock,
            keyboardOptions = KeyboardOptions(
                keyboardType = KeyboardType.Password,
                imeAction = ImeAction.Go
            ),
            keyboardActions = KeyboardActions(
                onGo = {
                    viewmodel.login(onSuccessLogin)
                }
            ),
            visualTransformation = PasswordVisualTransformation()
        )
        state.uiState.passwordError?.let { errorMsg ->
            Spacer(modifier = Modifier.height(spaceSmall))
            AppText(
                text = errorMsg,
                color = MaterialTheme.colorScheme.error,
                fontSize = errorTextFieldSize
            )
        }
    }
}