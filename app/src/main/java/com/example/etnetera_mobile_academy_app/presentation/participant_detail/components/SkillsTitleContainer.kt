package com.example.etnetera_mobile_academy_app.presentation.participant_detail.components

import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.width
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Settings
import androidx.compose.material3.MaterialTheme
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.stringResource
import com.example.etnetera_mobile_academy_app.Constants
import com.example.etnetera_mobile_academy_app.R
import com.example.etnetera_mobile_academy_app.domain.utils.getValueFromSharedPreferences
import com.example.etnetera_mobile_academy_app.presentation.participant_detail.AppDetailViewmodel
import com.example.etnetera_mobile_academy_app.presentation.participant_detail.DetailUIState
import com.example.etnetera_mobile_academy_app.presentation.utils.AppIcon
import com.example.etnetera_mobile_academy_app.presentation.utils.AppText
import com.example.etnetera_mobile_academy_app.ui.theme.AppIcons
import com.example.etnetera_mobile_academy_app.ui.theme.spaceSmall

@Composable
fun SkillsTitleContainer(
    viewmodel: AppDetailViewmodel,
    uiState: DetailUIState,
    modifier: Modifier = Modifier
) {
    val context = LocalContext.current
    val isFavorite = getValueFromSharedPreferences(context, Constants.CURRENT_USER_ID)

    Row(
        verticalAlignment = Alignment.CenterVertically,
        modifier = Modifier.then(modifier)
    ) {
        AppText(text = stringResource(R.string.skills_title))
        Spacer(modifier = Modifier.width(spaceSmall))

        if (isFavorite == uiState.participant?.id) {
            AppIcon(
                icon = Icons.Default.Settings,
                tint = MaterialTheme.colorScheme.onPrimary,
                iconSize = AppIcons.IconSizes.skillsSettingIconSize,
                onClick = {
                    viewmodel.openAndCloseSkillSettingSheet()
                    if(uiState.isSkillChangeSheetOpen){
                        viewmodel.cancelSkillUpdate()
                    }
                }
            )
        }
    }
}