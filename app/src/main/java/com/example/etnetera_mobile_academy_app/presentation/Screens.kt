package com.example.etnetera_mobile_academy_app.presentation

import android.os.Bundle
import com.example.etnetera_mobile_academy_app.Constants

sealed class Screens(val route: String) {
    object ListScreen : Screens("ParticipantList")

    object DetailScreen : Screens("ParticipantsDetail/{${Constants.PARTICIPANT_ID_ROUTE_ARG}}") {
        fun createRoute(id: String): String = "ParticipantsDetail/$id"

        fun getId(bundle: Bundle?): String = requireNotNull(bundle?.getString(Constants.PARTICIPANT_ID_ROUTE_ARG))
    }

    object LoginScreen : Screens("Login/{${Constants.PARTICIPANT_ID_ROUTE_ARG}}")
}