package com.example.etnetera_mobile_academy_app.presentation.participants_list.components

import androidx.compose.foundation.background
import androidx.compose.foundation.border
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material3.MaterialTheme
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.shadow
import androidx.compose.ui.text.font.FontWeight
import com.example.etnetera_mobile_academy_app.domain.models.participant.Participant
import com.example.etnetera_mobile_academy_app.presentation.utils.AppIcon
import com.example.etnetera_mobile_academy_app.presentation.utils.AppText
import com.example.etnetera_mobile_academy_app.presentation.utils.getParticipantTypeIcon
import com.example.etnetera_mobile_academy_app.ui.theme.borderLineMedium
import com.example.etnetera_mobile_academy_app.ui.theme.listItemAndFieldCornerShape
import com.example.etnetera_mobile_academy_app.ui.theme.listItemShadowColor
import com.example.etnetera_mobile_academy_app.ui.theme.listItemTextSize
import com.example.etnetera_mobile_academy_app.ui.theme.paddingSpaceLarge
import com.example.etnetera_mobile_academy_app.ui.theme.paddingSpaceMedium
import com.example.etnetera_mobile_academy_app.ui.theme.shadowElevationSmall
import com.example.etnetera_mobile_academy_app.ui.theme.spaceMedium

@Composable
fun ListItemContainer(
    participant: Participant,
    toDetailScreen: (String) -> Unit,
    modifier: Modifier = Modifier
) {
    Row(
        verticalAlignment = Alignment.CenterVertically,
        modifier = Modifier
            .fillMaxWidth()
            .shadow(
                elevation = shadowElevationSmall,
                spotColor = listItemShadowColor(color = MaterialTheme.colorScheme.onPrimary)
            )
            .border(
                width = borderLineMedium,
                color = MaterialTheme.colorScheme.primary,
                shape = RoundedCornerShape(listItemAndFieldCornerShape)
            )
            .background(
                color = MaterialTheme.colorScheme.surface,
                shape = RoundedCornerShape(listItemAndFieldCornerShape)
            )
            .clickable {
                toDetailScreen(participant.id)
            }
            .padding(horizontal = paddingSpaceMedium, vertical = paddingSpaceLarge)
            .then(modifier)
    ) {
        AppIcon(
            icon = getParticipantTypeIcon(participant.participantType),
            isClickable = false
        )

        Spacer(modifier = Modifier.width(spaceMedium))

        AppText(
            text = participant.name,
            fontSize = listItemTextSize,
            fontWeight = FontWeight.Bold,
            maxLines = 1,
            color = MaterialTheme.colorScheme.onSurface
        )
    }
}