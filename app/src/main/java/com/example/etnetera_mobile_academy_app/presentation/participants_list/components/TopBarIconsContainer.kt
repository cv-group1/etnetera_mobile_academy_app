package com.example.etnetera_mobile_academy_app.presentation.participants_list.components

import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.width
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.AccountCircle
import androidx.compose.material.icons.filled.List
import androidx.compose.material.icons.filled.Refresh
import androidx.compose.material3.MaterialTheme
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import com.example.etnetera_mobile_academy_app.presentation.participants_list.AppListViewmodel
import com.example.etnetera_mobile_academy_app.presentation.utils.AppIcon
import com.example.etnetera_mobile_academy_app.ui.theme.spaceSmall

@Composable
fun TopBarIconsContainer(
    viewmodel: AppListViewmodel,
    toLoginScreen: () -> Unit
) {
    val color = MaterialTheme.colorScheme.onPrimary

    AppIcon(
        icon = Icons.Default.Refresh,
        tint = color,
        onClick = { viewmodel.fetchParticipants() }
    )

    Spacer(modifier = Modifier.width(spaceSmall))
    AppIcon(
        icon = Icons.Default.List,
        tint = color,
        onClick = { viewmodel.openAndCloseFilterSheet() }
    )

    Spacer(modifier = Modifier.width(spaceSmall))
    AppIcon(
        icon = Icons.Default.AccountCircle,
        tint = color,
        onClick = toLoginScreen
    )
}