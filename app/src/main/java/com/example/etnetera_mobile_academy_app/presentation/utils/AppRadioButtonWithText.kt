package com.example.etnetera_mobile_academy_app.presentation.utils

import androidx.compose.foundation.interaction.MutableInteractionSource
import androidx.compose.foundation.layout.Row
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.RadioButton
import androidx.compose.material3.RadioButtonColors
import androidx.compose.material3.RadioButtonDefaults
import androidx.compose.runtime.Composable
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.unit.TextUnit
import com.example.etnetera_mobile_academy_app.ui.theme.filterRadioBtnSize

@Composable
fun AppRadioButtonWithText(
    selected: Boolean,
    text: UiText,
    modifier: Modifier = Modifier,
    onClick: (() -> Unit)? = null,
    enabled: Boolean = true,
    colorBtn: RadioButtonColors = RadioButtonDefaults.colors(),
    interactionSource: MutableInteractionSource = remember { MutableInteractionSource() },
    colorText: Color = MaterialTheme.colorScheme.onPrimary,
    fontSize: TextUnit = filterRadioBtnSize,
) {
    Row(
        verticalAlignment = Alignment.CenterVertically,
        modifier = modifier
    ) {
        RadioButton(
            selected = selected,
            onClick = onClick,
            enabled = enabled,
            colors = colorBtn,
            interactionSource = interactionSource
        )
        AppText(text = text, fontSize = fontSize, color = colorText)
    }
}

@Composable
fun AppRadioButtonWithText(
    selected: Boolean,
    text: String,
    modifier: Modifier = Modifier,
    onClick: (() -> Unit)? = null,
    enabled: Boolean = true,
    colorBtn: RadioButtonColors = RadioButtonDefaults.colors(),
    interactionSource: MutableInteractionSource = remember { MutableInteractionSource() },
    colorText: Color = MaterialTheme.colorScheme.onPrimary,
    fontSize: TextUnit = filterRadioBtnSize,
) {
    Row(
        verticalAlignment = Alignment.CenterVertically,
        modifier = modifier
    ) {
        RadioButton(
            selected = selected,
            onClick = onClick,
            enabled = enabled,
            colors = colorBtn,
            interactionSource = interactionSource
        )
        AppText(text = text, fontSize = fontSize, color = colorText)
    }
}