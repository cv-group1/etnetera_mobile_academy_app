package com.example.etnetera_mobile_academy_app.presentation.participants_list.components

import androidx.compose.foundation.ExperimentalFoundationApi
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.lazy.rememberLazyListState
import androidx.compose.material3.MaterialTheme
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import com.example.etnetera_mobile_academy_app.presentation.participants_list.AppListViewmodel
import com.example.etnetera_mobile_academy_app.presentation.participants_list.ListUiState
import com.example.etnetera_mobile_academy_app.ui.theme.paddingSpaceLarge
import com.example.etnetera_mobile_academy_app.ui.theme.paddingSpaceZero
import com.example.etnetera_mobile_academy_app.ui.theme.spaceMedium

@OptIn(ExperimentalFoundationApi::class)
@Composable
fun ListComponentsContainer(
    viewmodel: AppListViewmodel,
    uiState: ListUiState,
    toDetailScreen: (String) -> Unit,
    modifier: Modifier = Modifier,
    paddingValues: PaddingValues = PaddingValues(paddingSpaceZero)
) {
    val lazyListState = rememberLazyListState()

    Column(
        modifier
            .padding(paddingValues)
            .fillMaxWidth()
    ) {
        if(uiState.isFilterSheetOpen){
            FilterSheetContainer(
                viewmodel = viewmodel,
                uiState = uiState,
                lazyListState = lazyListState
            )
        }

        LazyColumn(
            verticalArrangement = Arrangement.spacedBy(spaceMedium),
            horizontalAlignment = Alignment.CenterHorizontally,
            contentPadding = PaddingValues(paddingSpaceLarge),
            state = lazyListState,
            modifier = Modifier
                .background(color = MaterialTheme.colorScheme.background)
                .fillMaxWidth()
        ) {
            items(uiState.participants, key = { it.id }) { participant ->
                ListItemContainer(
                    participant = participant,
                    toDetailScreen = toDetailScreen,
                    modifier = Modifier.animateItemPlacement()
                )
            }
        }
    }
}