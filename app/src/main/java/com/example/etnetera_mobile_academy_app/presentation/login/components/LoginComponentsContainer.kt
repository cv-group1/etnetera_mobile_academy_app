package com.example.etnetera_mobile_academy_app.presentation.login.components

import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.foundation.verticalScroll
import androidx.compose.material3.Button
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.shadow
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.font.FontWeight
import com.example.etnetera_mobile_academy_app.R
import com.example.etnetera_mobile_academy_app.presentation.login.AppLoginScreenViewmodel
import com.example.etnetera_mobile_academy_app.ui.theme.listItemAndFieldCornerShape
import com.example.etnetera_mobile_academy_app.ui.theme.paddingSpaceHuge
import com.example.etnetera_mobile_academy_app.ui.theme.paddingSpaceSmall
import com.example.etnetera_mobile_academy_app.ui.theme.shadowElevationSmall
import com.example.etnetera_mobile_academy_app.ui.theme.spaceMedium
import com.example.etnetera_mobile_academy_app.ui.theme.textFieldSize

@Composable
fun LoginComponentsContainer(
    viewmodel: AppLoginScreenViewmodel,
    state: AppLoginScreenViewmodel.State.Success,
    paddingValues: PaddingValues,
    onSuccessLogin: () -> Unit,
    modifier: Modifier = Modifier
) {
    Column(
        horizontalAlignment = Alignment.CenterHorizontally,
        verticalArrangement = Arrangement.Center,
        modifier = Modifier
            .padding(paddingValues)
            .fillMaxSize()
            .padding(paddingSpaceHuge)
            .verticalScroll(rememberScrollState())
            .then(modifier)
    ) {
        TextFieldsContainer(
            viewmodel = viewmodel,
            state = state,
            onSuccessLogin = onSuccessLogin
        )

        Spacer(modifier = Modifier.height(spaceMedium))

        Button(
            onClick = { viewmodel.login(onSuccessLogin) },
            shape = RoundedCornerShape(listItemAndFieldCornerShape),
            modifier = Modifier
                .fillMaxWidth()
                .shadow(shadowElevationSmall)
        ) {
            Text(
                text = stringResource(R.string.log_in_textfield_text),
                fontSize = textFieldSize,
                fontWeight = FontWeight.Bold,
                modifier = Modifier.padding(vertical = paddingSpaceSmall)
            )
        }
    }
}