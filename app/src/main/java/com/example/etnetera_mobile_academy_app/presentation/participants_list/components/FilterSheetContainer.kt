package com.example.etnetera_mobile_academy_app.presentation.participants_list.components

import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.lazy.LazyListState
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import com.example.etnetera_mobile_academy_app.presentation.participants_list.AppListViewmodel
import com.example.etnetera_mobile_academy_app.presentation.participants_list.ListUiState

@Composable
fun FilterSheetContainer(
    viewmodel: AppListViewmodel,
    uiState: ListUiState,
    lazyListState: LazyListState,
    modifier: Modifier = Modifier
) {
    Column(
        verticalArrangement = Arrangement.Center,
        horizontalAlignment = Alignment.CenterHorizontally,
        modifier = Modifier
            .fillMaxWidth()
            .then(modifier)
    ) {
        FilterRadioButtonsContainer(
            viewmodel = viewmodel,
            uiState = uiState,
            lazyListState = lazyListState,
            )

        SearchingFieldContainer(
            viewmodel = viewmodel,
            uiState = uiState,
            lazyListState = lazyListState,
            )
    }

}