package com.example.etnetera_mobile_academy_app.presentation.utils

import androidx.compose.runtime.Composable
import androidx.compose.ui.graphics.painter.Painter
import androidx.compose.ui.res.painterResource
import com.example.etnetera_mobile_academy_app.Constants
import com.example.etnetera_mobile_academy_app.ui.theme.AppIcons

@Composable
fun getSkillIcon(skillType : String): Painter {
    return when(skillType){
        Constants.ParticipantApi.ReturnTypes.SkillName.SWIFT -> painterResource(AppIcons.IconsSVG.swift)
        Constants.ParticipantApi.ReturnTypes.SkillName.IOS -> painterResource(AppIcons.IconsSVG.iOS)
        Constants.ParticipantApi.ReturnTypes.SkillName.KOTLIN -> painterResource(AppIcons.IconsSVG.kotlin)
        Constants.ParticipantApi.ReturnTypes.SkillName.ANDROID -> painterResource(AppIcons.IconsSVG.android)
        else -> painterResource(AppIcons.IconsSVG.Warning)
    }
}