package com.example.etnetera_mobile_academy_app.presentation.utils

import com.example.etnetera_mobile_academy_app.R

enum class InternalErrors(val uiText: UiText) {
    BLANK_FIELD(UiText.StringResource(R.string.error_msg_field_validation_blank_text)),
    USERNAME_INCORRECT_TEXT(UiText.StringResource(R.string.error_msg_email_validation_incorrect_email_text)),
    PASSWORD_INCORRECT_TEXT(UiText.StringResource(R.string.error_msg_password_validation_incorrect_password_text))
}