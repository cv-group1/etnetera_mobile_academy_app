package com.example.etnetera_mobile_academy_app.presentation.utils

import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.KeyboardArrowLeft
import androidx.compose.material3.MaterialTheme
import androidx.compose.runtime.Composable
import com.example.etnetera_mobile_academy_app.ui.theme.AppIcons

@Composable
fun GetBackNavigationIcon(onClick: () -> Unit){
    AppIcon(
        icon = Icons.Default.KeyboardArrowLeft,
        tint = MaterialTheme.colorScheme.onPrimary,
        iconSize = AppIcons.IconSizes.topBarBackIconSize,
        onClick = onClick
    )
}