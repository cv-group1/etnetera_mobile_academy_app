package com.example.etnetera_mobile_academy_app.presentation.participant_detail.components

import androidx.compose.foundation.border
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Favorite
import androidx.compose.material.icons.filled.FavoriteBorder
import androidx.compose.material3.CircularProgressIndicator
import androidx.compose.material3.IconToggleButton
import androidx.compose.material3.MaterialTheme
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.painterResource
import com.example.etnetera_mobile_academy_app.R
import com.example.etnetera_mobile_academy_app.presentation.participant_detail.AppDetailViewmodel
import com.example.etnetera_mobile_academy_app.presentation.participant_detail.DetailUIState
import com.example.etnetera_mobile_academy_app.presentation.utils.AppIcon
import com.example.etnetera_mobile_academy_app.presentation.utils.CoilAsyncImage
import com.example.etnetera_mobile_academy_app.presentation.utils.getAsyncImgPhoto
import com.example.etnetera_mobile_academy_app.ui.theme.AppIcons
import com.example.etnetera_mobile_academy_app.ui.theme.borderDetailPhoto
import com.example.etnetera_mobile_academy_app.ui.theme.spaceMedium

@Composable
fun ProfilPhotoContainer(
    viewmodel: AppDetailViewmodel,
    uiState: DetailUIState,
    modifier: Modifier = Modifier
){
    val context = LocalContext.current
    val checked = uiState.isFavorite
    val favoriteIcon = if(checked) Icons.Default.Favorite else Icons.Default.FavoriteBorder

    Row(
        verticalAlignment = Alignment.CenterVertically,
        horizontalArrangement = Arrangement.Center,
        modifier = Modifier
            .fillMaxWidth()
            .then(modifier)
    ) {
        Box(
            contentAlignment = Alignment.Center,
            modifier = Modifier.width(AppIcons.IconSizes.detailProfilePhotoContainerSize)
        ) {
            CoilAsyncImage(
                painter = getAsyncImgPhoto(url = uiState.participant?.icon512 ?: "", context = context),
                onErrorPlaceholder = {
                    AppIcon(
                        icon = painterResource(R.drawable.no_wifi_icon),
                        tint = MaterialTheme.colorScheme.onBackground,
                        iconSize = AppIcons.IconSizes.coilPlaceholderOnErrorIconSize
                    )
                },
                onLoadingPlaceholder = {
                    CircularProgressIndicator(
                        color = MaterialTheme.colorScheme.onBackground,
                        modifier = Modifier.size(AppIcons.IconSizes.coilPlaceholderOnLoadingIconSize)
                    )
                },
                modifier = Modifier
                    .padding(bottom = spaceMedium)
                    .clip(shape = CircleShape)
                    .border(borderDetailPhoto, MaterialTheme.colorScheme.primary, CircleShape)
                    .size(AppIcons.IconSizes.detailProfilePhotoSize)
            )

            IconToggleButton(
                checked = checked,
                onCheckedChange = {
                    if (!checked){
                        viewmodel.saveParticipantToDatabase()
                    } else {
                        viewmodel.deleteParticipantFromDatabase()
                    }
                },
                modifier = Modifier
                    .align(Alignment.BottomEnd)
                    .size(AppIcons.IconSizes.participantIconSize)
            ) {
                AppIcon(icon = favoriteIcon, isClickable = false)
            }
        }
    }
}