package com.example.etnetera_mobile_academy_app.presentation.utils

import androidx.compose.runtime.Composable
import androidx.compose.ui.graphics.painter.Painter
import androidx.compose.ui.res.painterResource
import com.example.etnetera_mobile_academy_app.domain.models.participant.ParticipantType
import com.example.etnetera_mobile_academy_app.ui.theme.AppIcons

@Composable
fun getParticipantTypeIcon(participantType: ParticipantType?): Painter =
    painterResource(participantType?.typeIcon ?: AppIcons.IconsSVG.emaSymbol)