package com.example.etnetera_mobile_academy_app.presentation.participant_detail.components

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.material3.MaterialTheme
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import com.example.etnetera_mobile_academy_app.R
import com.example.etnetera_mobile_academy_app.presentation.participant_detail.DetailUIState
import com.example.etnetera_mobile_academy_app.presentation.utils.AppIcon
import com.example.etnetera_mobile_academy_app.ui.theme.spaceLarge
import com.example.etnetera_mobile_academy_app.ui.theme.spaceMedium

@Composable
fun SocialIconsContainer(
    uiState: DetailUIState,
    onSlackClick: (String) -> Unit,
    onLinkedInClick: (String?) -> Unit,
    onMailClick: (String?, String?) -> Unit,
    modifier: Modifier = Modifier
) {
    val paddingValues = PaddingValues(spaceMedium)
    val backgroundColor = MaterialTheme.colorScheme.primary
    val iconColor = MaterialTheme.colorScheme.onPrimary

    val slackUrl = uiState.participant?.slackURL

    val email = stringResource(R.string.personal_emal_text)
    val name = uiState.participant?.name
    val subject = name?.let { stringResource(R.string.mail_intent_subject_text, it) }

    Box(
        contentAlignment = Alignment.Center,
        modifier = Modifier
            .fillMaxWidth()
            .background(color = MaterialTheme.colorScheme.onPrimary)
            .padding(spaceLarge)
            .then(modifier)
    ) {
        Row(
            horizontalArrangement = Arrangement.spacedBy(spaceLarge),
            verticalAlignment = Alignment.CenterVertically
        ) {
            AppIcon(
                icon = painterResource(R.drawable.icons_slack),
                paddingValues = paddingValues,
                backgroundColor = backgroundColor,
                tint = iconColor,
                onClick = {
                    slackUrl?.let { onSlackClick(it) }
                }
            )
            AppIcon(
                icon = painterResource(R.drawable.icon_mail),
                paddingValues = paddingValues,
                backgroundColor = backgroundColor,
                tint = iconColor,
                onClick = {onMailClick(email, subject) }
            )
            AppIcon(
                icon = painterResource(R.drawable.icons_linkedin),
                paddingValues = paddingValues,
                backgroundColor = backgroundColor,
                tint = iconColor,
                onClick = { onLinkedInClick(subject) }
            )
        }
    }
}


