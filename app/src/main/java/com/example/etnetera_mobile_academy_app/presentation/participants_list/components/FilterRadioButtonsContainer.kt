package com.example.etnetera_mobile_academy_app.presentation.participants_list.components

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.lazy.LazyListState
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material3.MaterialTheme
import androidx.compose.runtime.Composable
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import com.example.etnetera_mobile_academy_app.domain.models.participant.ParticipantType
import com.example.etnetera_mobile_academy_app.presentation.participants_list.AppListViewmodel
import com.example.etnetera_mobile_academy_app.presentation.participants_list.ListUiState
import com.example.etnetera_mobile_academy_app.presentation.utils.AppRadioButtonWithText
import com.example.etnetera_mobile_academy_app.ui.theme.cornerShapeMedium
import com.example.etnetera_mobile_academy_app.ui.theme.spaceLarge
import com.example.etnetera_mobile_academy_app.ui.theme.spaceMedium
import kotlinx.coroutines.launch

@Composable
fun FilterRadioButtonsContainer(
    viewmodel: AppListViewmodel,
    uiState: ListUiState,
    lazyListState: LazyListState,
    modifier: Modifier = Modifier
) {
    val color = MaterialTheme.colorScheme.primary.copy(alpha = 0.5f)
    val shape = RoundedCornerShape(bottomStart = cornerShapeMedium, bottomEnd = cornerShapeMedium)
    val scope = rememberCoroutineScope()

    Row(
        horizontalArrangement = Arrangement.SpaceBetween,
        verticalAlignment = Alignment.CenterVertically,
        modifier = Modifier
            .fillMaxWidth()
            .background(color = color, shape = shape)
            .padding(horizontal = spaceLarge, vertical = spaceMedium)
            .then(modifier)
    ) {
        val defaultFilter = ParticipantType.DEFAULT_ALL
        val iosFilter = ParticipantType.STUDENT_IOS
        val androidFilter = ParticipantType.STUDENT_ANDROID

        AppRadioButtonWithText(
            selected = uiState.filterType == defaultFilter,
            onClick = {
                viewmodel.filterParticipants(defaultFilter)
                scope.launch {
                    lazyListState.animateScrollToItem(index = 0)
                }
            },
            text = defaultFilter.typeName,
        )

        AppRadioButtonWithText(
            selected = uiState.filterType == iosFilter,
            onClick = {
                viewmodel.filterParticipants(iosFilter)
                scope.launch {
                    lazyListState.animateScrollToItem(index = 0)
                }
            },
            text = iosFilter.typeName
        )

        AppRadioButtonWithText(
            selected = uiState.filterType == androidFilter,
            onClick = {
                viewmodel.filterParticipants(androidFilter)
                scope.launch {
                    lazyListState.animateScrollToItem(index = 0)
                }
            },
            text = androidFilter.typeName
        )
    }
}