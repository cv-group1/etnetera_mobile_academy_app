package com.example.etnetera_mobile_academy_app.presentation.participant_detail.components

import com.example.etnetera_mobile_academy_app.Constants

fun getFormattedScoreText(value: Int): String = String.format("%2d/${Constants.MAXIMUM_SKILL_VALUE}", value)