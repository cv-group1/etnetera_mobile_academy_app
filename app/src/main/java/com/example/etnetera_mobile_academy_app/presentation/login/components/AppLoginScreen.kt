package com.example.etnetera_mobile_academy_app.presentation.login.components

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.padding
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Scaffold
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.font.FontWeight
import com.example.etnetera_mobile_academy_app.R
import com.example.etnetera_mobile_academy_app.presentation.login.AppLoginScreenViewmodel
import com.example.etnetera_mobile_academy_app.presentation.utils.AppText
import com.example.etnetera_mobile_academy_app.presentation.utils.AppTopBar
import com.example.etnetera_mobile_academy_app.presentation.utils.GetBackNavigationIcon
import com.example.etnetera_mobile_academy_app.presentation.utils.LoadingProgressBar
import com.example.etnetera_mobile_academy_app.ui.theme.paddingSpaceMedium
import com.example.etnetera_mobile_academy_app.ui.theme.topBarTitleSize

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun AppLoginScreen(
    viewmodel: AppLoginScreenViewmodel,
    state: AppLoginScreenViewmodel.State,
    onSuccessLogin: () -> Unit,
    modifier: Modifier = Modifier
) {
    Scaffold(
        modifier = Modifier
            .fillMaxSize()
            .background(color = MaterialTheme.colorScheme.background)
            .then(modifier),

        topBar = {
            AppTopBar(
                title = { GetLoginTitle() },
                navigationIcon = { GetBackNavigationIcon(onClick = onSuccessLogin) },
                backgroundColor = MaterialTheme.colorScheme.primary
            )
        },
    ) { paddingValues ->
        when (state) {
            AppLoginScreenViewmodel.State.Loading -> {
                LoadingProgressBar(paddingValues = paddingValues)
            }

            is AppLoginScreenViewmodel.State.Error -> {
                val msg = state.error.message
                msg?.let {
                    Column(
                        modifier = Modifier.padding(paddingValues)
                    ) {
                        AppText(text = it, color = MaterialTheme.colorScheme.error)
                    }
                }
            }

            is AppLoginScreenViewmodel.State.Success -> {
                LoginComponentsContainer(
                    viewmodel = viewmodel,
                    state = state,
                    paddingValues = paddingValues,
                    onSuccessLogin = onSuccessLogin
                )
            }
        }
    }
}

@Composable
private fun GetLoginTitle() {
    AppText(
        text = stringResource(R.string.login_top_bar_title),
        fontSize = topBarTitleSize,
        fontWeight = FontWeight.Bold,
        modifier = Modifier.padding(start = paddingSpaceMedium)
    )
}