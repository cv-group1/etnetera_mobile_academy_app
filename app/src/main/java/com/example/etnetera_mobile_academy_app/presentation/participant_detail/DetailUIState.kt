package com.example.etnetera_mobile_academy_app.presentation.participant_detail

import com.example.etnetera_mobile_academy_app.domain.models.participant.Participant
import com.example.etnetera_mobile_academy_app.domain.models.participant.Skill

data class DetailUIState(
    val participant: Participant? = null,
    val isLoggedIn: Boolean = false,
    val isFavorite: Boolean = false,
    val isSkillChangeSheetOpen: Boolean = false,
    val skills: List<Skill> = emptyList(),
    val backupSkills: List<Skill> = emptyList()
)
