package com.example.etnetera_mobile_academy_app.presentation.login

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.etnetera_mobile_academy_app.Constants
import com.example.etnetera_mobile_academy_app.domain.use_case.login.LoginUseCasesContainer
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.flow.update
import kotlinx.coroutines.launch

class AppLoginScreenViewmodel(
    private val loginUseCasesContainer: LoginUseCasesContainer
): ViewModel(){

    private val _state = MutableStateFlow<State>(State.Success(LoginUiState()))
    val state = _state.asStateFlow()

    init {
        viewModelScope.launch {
            _state.update { state ->
                (state as State.Success).copy(
                    uiState = state.uiState.copy(
                        usernameText = Constants.ParticipantApi.MY_USERNAME,
                        passwordText = Constants.ParticipantApi.MY_PASSWORD
                    )
                )
            }
        }
    }

    fun login(onSuccessLogin: () -> Unit) {
        viewModelScope.launch {
            val uiStateResult = loginUseCasesContainer.loginUseCase(
                state = (_state.value as State.Success).uiState,
                onSuccessLogin = onSuccessLogin
            )
            _state.value = uiStateResult
        }
    }

    fun emailFieldTextChange(text: String){
        viewModelScope.launch {
            _state.update {
                (it as State.Success).copy(
                    uiState = it.uiState.copy(
                        usernameText = text
                    )
                )
            }
        }
    }

    fun passwordFieldTextChange(text: String) {
        viewModelScope.launch {
            _state.update {
                (it as State.Success).copy(
                    uiState = it.uiState.copy(
                        passwordText = text
                    )
                )
            }
        }
    }

    sealed interface State {
        object Loading : State
        data class Success(val uiState: LoginUiState) : State
        data class Error(val error: Throwable) : State
    }
}