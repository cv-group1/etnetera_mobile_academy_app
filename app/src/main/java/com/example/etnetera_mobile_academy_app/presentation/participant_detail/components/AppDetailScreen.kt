package com.example.etnetera_mobile_academy_app.presentation.participant_detail.components

import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.AccountCircle
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Scaffold
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import com.example.etnetera_mobile_academy_app.R
import com.example.etnetera_mobile_academy_app.presentation.participant_detail.AppDetailViewmodel
import com.example.etnetera_mobile_academy_app.presentation.utils.AppIcon
import com.example.etnetera_mobile_academy_app.presentation.utils.AppText
import com.example.etnetera_mobile_academy_app.presentation.utils.AppTopBar
import com.example.etnetera_mobile_academy_app.presentation.utils.GetBackNavigationIcon
import com.example.etnetera_mobile_academy_app.presentation.utils.LoadingProgressBar
import com.example.etnetera_mobile_academy_app.ui.theme.AppIcons
import com.example.etnetera_mobile_academy_app.ui.theme.paddingSpaceMedium

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun AppDetailScreen(
    viewmodel: AppDetailViewmodel,
    state: AppDetailViewmodel.State,
    toLoginScreen: () -> Unit,
    participantID: String,
    onBackClick: () -> Unit,
    onSlackClick: (String) -> Unit,
    onLinkedInClick: (String?) -> Unit,
    onMailClick: (String?, String?) -> Unit,
    modifier: Modifier = Modifier
) {

    Scaffold(
        modifier = Modifier
            .fillMaxSize()
            .background(color = MaterialTheme.colorScheme.background)
            .then(modifier),

        topBar = {
            AppTopBar(
                title = { GetEMALogo() },
                actions = { GetLoginIcon(toLoginScreen = toLoginScreen) },
                navigationIcon = { GetBackNavigationIcon(onClick = onBackClick) },
                backgroundColor = MaterialTheme.colorScheme.background
            )
        },
    ) { paddingValues ->
        when(state){
            AppDetailViewmodel.State.Loading -> {
                LoadingProgressBar(paddingValues = paddingValues)
                viewmodel.fetchParticipant(participantID)
            }

            is AppDetailViewmodel.State.Error -> {

                val msg = state.error.message
                msg?.let {
                    Column(
                        modifier = Modifier.padding(paddingValues)
                    ) {
                        AppText(text = it, color = MaterialTheme.colorScheme.error)
                    }
                }
            }

            is AppDetailViewmodel.State.Success -> {
                DetailComponentsContainer(
                    viewmodel = viewmodel,
                    state = state,
                    paddingValues = paddingValues,
                    onSlackClick = onSlackClick,
                    onLinkedInClick = onLinkedInClick,
                    onMailClick = onMailClick
                )
            }
        }
    }
}

@Composable
private fun GetLoginIcon(
    toLoginScreen: () -> Unit,
    modifier: Modifier = Modifier
) {
    AppIcon(
        icon = Icons.Default.AccountCircle,
        tint = MaterialTheme.colorScheme.onPrimary,
        modifier = modifier,
        onClick = toLoginScreen
    )
}

@Composable
private fun GetEMALogo(modifier: Modifier = Modifier) {
    Image(
        painter = painterResource(AppIcons.IconsPNG.emaLogo),
        contentDescription = stringResource(R.string.etnetera_logo_description),
        modifier = Modifier
            .padding(start = paddingSpaceMedium)
            .size(AppIcons.IconSizes.etneteraTopBarSize)
            .then(modifier)
    )
}

