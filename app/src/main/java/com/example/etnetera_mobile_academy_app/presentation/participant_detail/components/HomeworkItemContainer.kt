package com.example.etnetera_mobile_academy_app.presentation.participant_detail.components

import androidx.compose.foundation.background
import androidx.compose.foundation.border
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.defaultMinSize
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material3.MaterialTheme
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.draw.shadow
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.font.FontWeight
import com.example.etnetera_mobile_academy_app.Constants
import com.example.etnetera_mobile_academy_app.domain.models.participant.Homework
import com.example.etnetera_mobile_academy_app.presentation.utils.AppIcon
import com.example.etnetera_mobile_academy_app.presentation.utils.AppText
import com.example.etnetera_mobile_academy_app.ui.theme.AppIcons
import com.example.etnetera_mobile_academy_app.ui.theme.*


@Composable
fun HomeworkItemContainer(
    homework: Homework,
    modifier: Modifier = Modifier
) {
    Column(
        verticalArrangement = Arrangement.spacedBy(spaceLarge),
        modifier = Modifier
            .shadow(elevation = shadowElevationSmall)
            .background(color = Color.White, shape = RoundedCornerShape(cornerShapeMedium))
            .border(width = borderLineMedium, color = MaterialTheme.colorScheme.primary, shape = RoundedCornerShape(
                cornerShapeMedium
            )
            )
            .defaultMinSize(homeworkMinimumWidth)
            .padding(paddingSpaceLarge)
            .then(modifier)
    ) {
        Box(
            contentAlignment = Alignment.Center,
            modifier = Modifier
                .size(homeworkNumberContainerSize)
                .clip(shape = CircleShape)
                .background(color = MaterialTheme.colorScheme.primary)
        ) {
            AppText(
                text = "${homework.number}",
                fontSize = homeworkNumberSize,
                fontWeight = FontWeight.Bold
            )
        }

        Row(verticalAlignment = Alignment.CenterVertically) {
            AppText(text = homework.state, fontSize = homeworkStatusSize)

            if(homework.state == Constants.ParticipantApi.ReturnTypes.HomeworkState.ACCEPTANCE){
                Spacer(modifier = Modifier.width(spaceLarge))
                AppIcon(
                    icon = painterResource(AppIcons.IconsSVG.statusOK),
                    iconSize = AppIcons.IconSizes.homeworkStatusIconSize,
                    tint = Color.Green,
                    isClickable = false
                )
            }
        }
    }
}