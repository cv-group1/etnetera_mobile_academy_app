package com.example.etnetera_mobile_academy_app.presentation.participant_detail.components

import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.verticalScroll
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import com.example.etnetera_mobile_academy_app.presentation.participant_detail.AppDetailViewmodel
import com.example.etnetera_mobile_academy_app.ui.theme.spaceLarge
import com.example.etnetera_mobile_academy_app.ui.theme.spaceMedium

@Composable
fun DetailComponentsContainer(
    viewmodel: AppDetailViewmodel,
    state: AppDetailViewmodel.State.Success,
    paddingValues: PaddingValues,
    onSlackClick: (String) -> Unit,
    onLinkedInClick: (String?) -> Unit,
    onMailClick: (String?, String?) -> Unit,
    modifier: Modifier = Modifier
) {
    val uiState = state.uiState

    Column(
        horizontalAlignment = Alignment.CenterHorizontally,
        verticalArrangement = Arrangement.Top,
        modifier = Modifier
            .padding(paddingValues)
            .fillMaxWidth()
            .verticalScroll(rememberScrollState())
            .then(modifier)
    ) {
        Spacer(modifier = Modifier.height(spaceLarge))
        ProfilPhotoContainer(
            viewmodel = viewmodel,
            uiState = uiState
        )

        Spacer(modifier = Modifier.height(spaceMedium))
        NameContainer(uiState = uiState)

        Spacer(modifier = Modifier.height(spaceMedium))
        SocialIconsContainer(
            uiState = uiState,
            onSlackClick = onSlackClick,
            onLinkedInClick = onLinkedInClick,
            onMailClick = onMailClick
        )

        SkillsContainer(
            viewmodel = viewmodel,
            uiState = uiState
        )

        Spacer(modifier = Modifier.height(spaceMedium))
        HomeworksContainer(uiState = uiState)
    }
}