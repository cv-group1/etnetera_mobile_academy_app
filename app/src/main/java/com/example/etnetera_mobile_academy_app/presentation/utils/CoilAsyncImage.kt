package com.example.etnetera_mobile_academy_app.presentation.utils

import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.BoxScope
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.ColorFilter
import androidx.compose.ui.graphics.DefaultAlpha
import androidx.compose.ui.layout.ContentScale
import coil.compose.AsyncImagePainter

@Composable
fun CoilAsyncImage(
    painter: AsyncImagePainter,
    modifier: Modifier = Modifier,
    onLoadingPlaceholder: @Composable BoxScope.() -> Unit = {},
    onErrorPlaceholder: @Composable BoxScope.() -> Unit = {},
    onEmptyPlaceholder: @Composable BoxScope.() -> Unit = {},
    contentdescription: String? = null,
    alignment: Alignment = Alignment.TopCenter,
    contentscale: ContentScale = ContentScale.Fit,
    alpha: Float = DefaultAlpha,
    colorfilter: ColorFilter? = null,
) {
    Box(
        modifier = modifier,
        contentAlignment = Alignment.Center
    ) {
        Image(
            painter = painter,
            contentDescription = contentdescription,
            alignment = alignment,
            contentScale = contentscale,
            alpha = alpha,
            colorFilter = colorfilter,
            modifier = Modifier.matchParentSize()
        )

        when (painter.state) {
            is AsyncImagePainter.State.Loading -> onLoadingPlaceholder()
            is AsyncImagePainter.State.Error -> onErrorPlaceholder()
            AsyncImagePainter.State.Empty -> onEmptyPlaceholder()
            else -> Unit
        }
    }
}