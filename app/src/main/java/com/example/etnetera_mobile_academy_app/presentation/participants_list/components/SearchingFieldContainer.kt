package com.example.etnetera_mobile_academy_app.presentation.participants_list.components

import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.lazy.LazyListState
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Search
import androidx.compose.runtime.Composable
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.input.ImeAction
import androidx.compose.ui.text.input.KeyboardType
import com.example.etnetera_mobile_academy_app.R
import com.example.etnetera_mobile_academy_app.presentation.participants_list.AppListViewmodel
import com.example.etnetera_mobile_academy_app.presentation.participants_list.ListUiState
import com.example.etnetera_mobile_academy_app.presentation.utils.AppTextField
import com.example.etnetera_mobile_academy_app.ui.theme.paddingSpaceMedium
import com.example.etnetera_mobile_academy_app.ui.theme.paddingSpaceZero
import kotlinx.coroutines.launch

@Composable
fun SearchingFieldContainer(
    viewmodel: AppListViewmodel,
    uiState: ListUiState,
    lazyListState: LazyListState,
    modifier: Modifier = Modifier
) {
    val scope = rememberCoroutineScope()

    AppTextField(
        value = uiState.searchFieldText,
        onValueChange = {
            viewmodel.onSearchTextChange(it)
            scope.launch {
                lazyListState.animateScrollToItem(index = 0)
            }
        },
        label = stringResource(R.string.list_search_field_text),
        leadingIcon = Icons.Default.Search,
        keyboardOptions = KeyboardOptions(
            imeAction = ImeAction.Done,
            keyboardType = KeyboardType.Ascii
        ),
        modifier = Modifier
            .padding(paddingSpaceMedium, paddingSpaceZero)
            .then(modifier)
    )
}