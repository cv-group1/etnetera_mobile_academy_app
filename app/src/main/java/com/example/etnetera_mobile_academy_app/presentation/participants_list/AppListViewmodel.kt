package com.example.etnetera_mobile_academy_app.presentation.participants_list

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.etnetera_mobile_academy_app.R
import com.example.etnetera_mobile_academy_app.domain.models.participant.ParticipantType
import com.example.etnetera_mobile_academy_app.domain.repository.ParticipantRepository
import com.example.etnetera_mobile_academy_app.domain.use_case.list.ListUseCaseContainer
import com.example.etnetera_mobile_academy_app.presentation.utils.UiText
import kotlinx.coroutines.Job
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.flow.update
import kotlinx.coroutines.launch

class AppListViewmodel(
    private val repository: ParticipantRepository,
    private val listUseCases: ListUseCaseContainer
) : ViewModel() {
    private val _state = MutableStateFlow<State>(State.Loading)
    val state = _state.asStateFlow()

    private var searchJob: Job? = null

    init {
        viewModelScope.launch {
            fetchParticipants()
        }
    }

    fun onSearchTextChange(text: String) {
        searchJob?.cancel()
        searchJob = viewModelScope.launch {
            _state.update { state ->
                (state as State.Success).copy(
                    uiState = state.uiState.copy(
                        searchFieldText = text
                    )
                )
            }
            delay(500L)
            _state.update { state ->
                (state as State.Success).copy(
                    uiState = listUseCases.listSearchUseCase(state.uiState, text)
                )
            }
        }
    }

    fun fetchParticipants() {
        viewModelScope.launch {
            _state.value = State.Loading
            repository.getAllParticipants().fold(
                onSuccess = { participants ->
                    _state.value = State.Success(
                        ListUiState(
                            participants = participants,
                            participantsBackup = participants
                        )
                    )
                },
                onFailure = { error ->
                    _state.value = State.Error(UiText.DynamicString(error.message.toString()))
                }
            )
        }
    }

    fun openAndCloseFilterSheet() {
        viewModelScope.launch {
            if (_state.value is State.Success) {
                _state.update { state ->
                    (state as State.Success).copy(
                        uiState = state.uiState.copy(
                            isFilterSheetOpen = !state.uiState.isFilterSheetOpen
                        )
                    )
                }
            } else {
                _state.value =
                    State.Error(UiText.StringResource(R.string.list_error_msg_no_participants))
            }
        }
    }

    fun filterParticipants(filterType: ParticipantType) {
        viewModelScope.launch {
            if (_state.value is State.Success) {
                _state.update { state ->
                    (state as State.Success).copy(
                        uiState = listUseCases.listFilterUseCase(state.uiState, filterType)
                    )
                }
            } else {
                _state.value =
                    State.Error(UiText.StringResource(R.string.list_error_msg_no_participants))
            }
        }
    }


    sealed interface State {
        object Loading : State
        data class Success(val uiState: ListUiState) : State
        data class Error(val error: UiText) : State
    }
}