package com.example.etnetera_mobile_academy_app.presentation

import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.lifecycle.compose.collectAsStateWithLifecycle
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.compose.navigation
import androidx.navigation.compose.rememberNavController
import com.example.etnetera_mobile_academy_app.Constants
import com.example.etnetera_mobile_academy_app.presentation.login.AppLoginScreenViewmodel
import com.example.etnetera_mobile_academy_app.presentation.login.components.AppLoginScreen
import com.example.etnetera_mobile_academy_app.presentation.participant_detail.AppDetailViewmodel
import com.example.etnetera_mobile_academy_app.presentation.participant_detail.components.AppDetailScreen
import com.example.etnetera_mobile_academy_app.presentation.participants_list.AppListViewmodel
import com.example.etnetera_mobile_academy_app.presentation.participants_list.components.AppListScreen
import org.koin.androidx.compose.getViewModel

@Composable
fun NavGraph(
    onSlackClick: (String) -> Unit,
    onLinkedInClick: (String?) -> Unit,
    onMailClick: (String?, String?) -> Unit
) {
    val navController = rememberNavController()

    NavHost(navController = navController, startDestination = Screens.ListScreen.route) {
        composable(Screens.ListScreen.route) {
            val viewmodel = getViewModel<AppListViewmodel>()
            val state by viewmodel.state.collectAsStateWithLifecycle()

            AppListScreen(
                viewmodel = viewmodel,
                state = state,
                toDetailScreen = { navController.navigate(Screens.DetailScreen.createRoute(it)) },
                toLoginScreen = { navController.navigate(Screens.LoginScreen.route) }
            )
        }

        navigation(startDestination = Screens.DetailScreen.route, route = Constants.DETAIL_FEATURE){
            composable(Screens.DetailScreen.route) {
                val viewmodel = getViewModel<AppDetailViewmodel>()
                val state by viewmodel.state.collectAsStateWithLifecycle()
                val args = it.arguments

                AppDetailScreen(
                    viewmodel = viewmodel,
                    state = state,
                    toLoginScreen = { navController.navigate(Screens.LoginScreen.route) },
                    participantID = Screens.DetailScreen.getId(args),
                    onBackClick = navController::popBackStack,
                    onSlackClick = onSlackClick,
                    onLinkedInClick = onLinkedInClick,
                    onMailClick = onMailClick
                )
            }
        }

        navigation(startDestination = Screens.LoginScreen.route, route = Constants.LOGIN_FEATURE){
            composable(Screens.LoginScreen.route) {
                val viewmodel = getViewModel<AppLoginScreenViewmodel>()
                val state by viewmodel.state.collectAsStateWithLifecycle()

                AppLoginScreen(
                    viewmodel = viewmodel,
                    state = state,
                    onSuccessLogin = {
                        navController.navigate(Screens.ListScreen.route){
                            popUpTo(Constants.LOGIN_FEATURE){
                                inclusive = true
                            }
                        }
                    }
                )
            }
        }
    }
}
