package com.example.etnetera_mobile_academy_app.presentation.participant_detail.components

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.shadow
import androidx.compose.ui.graphics.Color
import com.example.etnetera_mobile_academy_app.presentation.participant_detail.AppDetailViewmodel
import com.example.etnetera_mobile_academy_app.presentation.participant_detail.DetailUIState
import com.example.etnetera_mobile_academy_app.ui.theme.cornerShapeLarge
import com.example.etnetera_mobile_academy_app.ui.theme.shadowElevationSmall
import com.example.etnetera_mobile_academy_app.ui.theme.spaceLarge
import com.example.etnetera_mobile_academy_app.ui.theme.spaceMedium

@Composable
fun SkillsContainer(
    viewmodel: AppDetailViewmodel,
    uiState: DetailUIState,
    modifier: Modifier = Modifier
) {
    val shape = RoundedCornerShape(bottomStart = cornerShapeLarge, bottomEnd = cornerShapeLarge)
    Column(
        verticalArrangement = Arrangement.Center,
        modifier = Modifier
            .fillMaxWidth()
            .shadow(elevation = shadowElevationSmall, shape = shape)
            .background(color = Color.White, shape = shape)
            .padding(spaceLarge)
            .then(modifier)
    ) {
        SkillsTitleContainer(
            viewmodel = viewmodel,
            uiState = uiState
        )
        Spacer(modifier = Modifier.height(spaceMedium))

            if (uiState.isSkillChangeSheetOpen) {
                UpdateSkillsValuesContainer(
                    viewmodel = viewmodel,
                    skills = uiState.skills
                )
            } else {
                SkillsOverviewContainer(skills = uiState.skills)
            }
        }
    }