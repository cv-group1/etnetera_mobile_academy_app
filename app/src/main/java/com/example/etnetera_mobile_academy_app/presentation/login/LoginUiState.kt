package com.example.etnetera_mobile_academy_app.presentation.login

import com.example.etnetera_mobile_academy_app.presentation.utils.UiText

data class LoginUiState(
    val usernameText: String = "",
    val passwordText: String = "",
    val usernameError: UiText? = null,
    val passwordError: UiText? = null
)
