package com.example.etnetera_mobile_academy_app.presentation.participants_list.components

import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.padding
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Scaffold
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.font.FontWeight
import com.example.etnetera_mobile_academy_app.R
import com.example.etnetera_mobile_academy_app.presentation.participants_list.AppListViewmodel
import com.example.etnetera_mobile_academy_app.presentation.utils.AppText
import com.example.etnetera_mobile_academy_app.presentation.utils.AppTopBar
import com.example.etnetera_mobile_academy_app.presentation.utils.LoadingProgressBar
import com.example.etnetera_mobile_academy_app.ui.theme.topBarTitleSize

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun AppListScreen(
    viewmodel: AppListViewmodel,
    state: AppListViewmodel.State,
    toDetailScreen: (String) -> Unit,
    toLoginScreen: () -> Unit,
    modifier: Modifier = Modifier
) {
    Scaffold(
        modifier = Modifier
            .fillMaxSize()
            .then(modifier),

        topBar = {
            AppTopBar(
                title = { GetListTitle() },
                actions = {
                    TopBarIconsContainer(
                        viewmodel = viewmodel,
                        toLoginScreen = toLoginScreen
                    )
                }
            )
        }

    ) { paddingValues ->
        when (state) {
            AppListViewmodel.State.Loading -> {
                LoadingProgressBar(paddingValues = paddingValues)
            }

            is AppListViewmodel.State.Error -> {
                Column(
                    modifier = Modifier.padding(paddingValues)
                ) {
                    AppText(text = state.error, color = MaterialTheme.colorScheme.error)
                }
            }

            is AppListViewmodel.State.Success -> {
                ListComponentsContainer(
                    viewmodel = viewmodel,
                    uiState = state.uiState,
                    toDetailScreen = toDetailScreen,
                    paddingValues = paddingValues
                )
            }
        }
    }
}

@Composable
private fun GetListTitle() {
    AppText(
        text = stringResource(R.string.top_bar_title_text),
        fontSize = topBarTitleSize,
        fontWeight = FontWeight.Bold
    )
}