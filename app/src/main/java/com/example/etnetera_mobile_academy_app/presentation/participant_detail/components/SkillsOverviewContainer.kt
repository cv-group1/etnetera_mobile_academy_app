package com.example.etnetera_mobile_academy_app.presentation.participant_detail.components

import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material3.LinearProgressIndicator
import androidx.compose.material3.MaterialTheme
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.text.font.FontWeight
import com.example.etnetera_mobile_academy_app.domain.models.participant.Skill
import com.example.etnetera_mobile_academy_app.presentation.utils.AppIcon
import com.example.etnetera_mobile_academy_app.presentation.utils.AppText
import com.example.etnetera_mobile_academy_app.presentation.utils.getSkillIcon
import com.example.etnetera_mobile_academy_app.presentation.utils.getSkillValueForSlider
import com.example.etnetera_mobile_academy_app.ui.theme.cornerShapeMedium
import com.example.etnetera_mobile_academy_app.ui.theme.progressIdicatorHeight
import com.example.etnetera_mobile_academy_app.ui.theme.skillScoreTextSize
import com.example.etnetera_mobile_academy_app.ui.theme.spaceMedium

@Composable
fun SkillsOverviewContainer(
    skills: List<Skill>,
    modifier: Modifier = Modifier
) {
    skills.forEach { skill ->
        Row(
            verticalAlignment = Alignment.CenterVertically,
            horizontalArrangement = Arrangement.spacedBy(spaceMedium),
            modifier = Modifier
                .fillMaxWidth()
                .then(modifier)
        ) {
            AppIcon(
                icon = getSkillIcon(skill.skillType),
                isClickable = false,
                tint = Color.Unspecified
            )
            LinearProgressIndicator(
                progress = getSkillValueForSlider(skill.value),
                color = MaterialTheme.colorScheme.onPrimary,
                modifier = Modifier
                    .weight(1f)
                    .height(progressIdicatorHeight)
                    .clip(RoundedCornerShape(cornerShapeMedium))
            )
            AppText(
                text = getFormattedScoreText(skill.value),
                fontSize = skillScoreTextSize,
                fontWeight = FontWeight.Bold
            )
        }
    }
}