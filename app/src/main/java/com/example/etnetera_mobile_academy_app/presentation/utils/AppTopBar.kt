package com.example.etnetera_mobile_academy_app.presentation.utils

import androidx.compose.foundation.layout.RowScope
import androidx.compose.foundation.layout.WindowInsets
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.TopAppBar
import androidx.compose.material3.TopAppBarDefaults
import androidx.compose.material3.TopAppBarScrollBehavior
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.shadow
import androidx.compose.ui.graphics.Color
import com.example.etnetera_mobile_academy_app.ui.theme.shadowElevationLarge

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun AppTopBar(
    modifier: Modifier = Modifier,
    title: @Composable () -> Unit = {},
    navigationIcon: @Composable () -> Unit = {},
    windowInsets: WindowInsets = TopAppBarDefaults.windowInsets,
    scrollBehavior: TopAppBarScrollBehavior? = null,
    actions: @Composable RowScope.() -> Unit = {},
    titleColor: Color = MaterialTheme.colorScheme.onPrimary,
    backgroundColor: Color = MaterialTheme.colorScheme.primary
) {
    TopAppBar(
        title = title,
        actions = actions,
        colors = TopAppBarDefaults.smallTopAppBarColors(
            containerColor = backgroundColor,
            titleContentColor = titleColor,
        ),
        navigationIcon = navigationIcon,
        windowInsets = windowInsets,
        scrollBehavior = scrollBehavior,
        modifier = Modifier
            .shadow(elevation = shadowElevationLarge)
            .then(modifier)
    )
}