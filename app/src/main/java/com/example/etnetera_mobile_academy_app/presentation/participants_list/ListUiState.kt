package com.example.etnetera_mobile_academy_app.presentation.participants_list

import com.example.etnetera_mobile_academy_app.domain.models.participant.Participant
import com.example.etnetera_mobile_academy_app.domain.models.participant.ParticipantType

data class ListUiState(
    val participants: List<Participant>,
    val participantsBackup: List<Participant> = emptyList(),
    val searchFieldText: String = "",
    val isFilterSheetOpen: Boolean = false,
    val isLoginSheetOpen: Boolean = false,
    val filterType: ParticipantType = ParticipantType.DEFAULT_ALL
)
